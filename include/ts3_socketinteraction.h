#pragma once

#include "teamspeak\public_definitions.h"
#include "teamspeak\public_rare_definitions.h"
#include <stdint.h>

const int BUFFERLENGTH = 1024;
const int BUFFERLENGTH2 = 65536;
static const char * CUSTROMMICRO_DEVICEID = "MusicDreamDeviceId";
static const char * CUSTROMMICRO_DEVICENAME = "Music Dream Musik";



enum PID
{
	EMPTY = 0x00, STARTCONNECTION, STOPCONNECTION, CONNECTIONSTATE, CLIENTDATA, CLIENTACTION, CHANNELDATA, SERVERDATA, REVMESSAGE, SENDMESSAGE, ACTIVATEAUDIO, AUDIODATA
};

enum TSConnectStatus
{
	TSSTATUS_DISCONNECTED = 0x00, TSSTATUS_CONNECTING = 0x01, TSSTATUS_CONNECTED = 0x02, TSSTATUS_CONNECTION_ESTABLISHING = 0x03, TSSTATUS_CONNECTION_ESTABLISHED = 0x4
};

enum ClientAction
{
	MOVE = 0x00, LEAVEVISIBILITY 
};

enum MessageType {
	PRIVATE = 1, CHANNEL = 2
};




#pragma pack(push, 1)

struct PackageData_t {
	PID _PID;
};

struct PackageHeader_t {
	PID _PID;
	uint16_t length;
};
const int HEADERSIZE = sizeof(PackageHeader_t);



struct PackageUserMessage : PackageData_t {
	char user[28];
	char message[1024];
};

struct PackageStartConnect : PackageData_t
{
	char IpAddress[255 + 1];
	uint16_t Port;
	char Identity[255 + 1];
	char Nickname[28 + 1];
	char ServerPassword[40 + 1];
};

struct PackageStopConnect : PackageData_t
{
	UINT64 ServerConnectionHandlerID;
	char ExitMsg[TS3_MAX_SIZE_AWAY_MESSAGE];
};

struct PackageRevMessage : PackageData_t
{
	UINT64 serverConnectionHandlerID;
	MessageType MessageType;
	UINT64 TargetID;
	char UniqueIdentifier[28 + 1];
	char Message[TS3_MAX_SIZE_TEXTMESSAGE + 1];
};

struct PackageSendMessage : PackageData_t
{
	UINT64 serverConnectionHandlerID;
	MessageType MessageType;
	UINT64 TargetID;
	char Message[TS3_MAX_SIZE_TEXTMESSAGE + 1];
};

struct PackageActivateAudio : PackageData_t
{
	UINT64 serverConnectionHandlerID;
	TSConnectStatus ConnectionState;
};




struct PackageClientData : PackageData_t
{
	UINT64 serverConnectionHandlerID;
	UINT16 ClientId;
	UINT64 CurrentChannelID;
	char UniqueIdentifier[28 + 1];
	char Nickname[TS3_MAX_SIZE_CLIENT_NICKNAME + 1];
	bool FlagSelf;
};

struct PackageClientAction : PackageData_t
{
	UINT64 serverConnectionHandlerID;
	UINT16 ClientId;
	UINT64 oldChannelID;
	UINT64 newChannelID;
	ClientAction Action;
};




struct PackageChannelData : PackageData_t
{
	UINT64 serverConnectionHandlerID;
	UINT64 ChannelId;
	UINT64 ChannelParentId;
	char Name[TS3_MAX_SIZE_CHANNEL_NAME + 1];
	char Topic[TS3_MAX_SIZE_CHANNEL_TOPIC + 1];
	char Description[TS3_MAX_SIZE_CHANNEL_DESCRIPTION + 1];
	int NeededTalkPower;
	int Codec;
	int CodecQuality;
	int CodecLatencyFactor;
	bool CodecIsUnencrypted;
	int MaxClients;
	int MaxFamilyClients;
	int Order;
	bool FlagPermanent;
	bool FlagSemiPermanent;
	bool FlagDefault;
	bool FlagPassword; 
	bool FlagSubscribed;
};




struct PackageServerData : PackageData_t
{
	UINT64 serverConnectionHandlerID;
	char UniqueIdentifier[28 + 1];
	char Name[TS3_MAX_SIZE_VIRTUALSERVER_NAME + 1];
	char WelcomeMessage[TS3_MAX_SIZE_VIRTUALSERVER_WELCOMEMESSAGE + 1];
	char Platform[16 + 1];
	char Version[32 + 1];
	int MaxClients;
	bool CodecEncriptionMode;

	//char Platform[TS3_MAX_SIZE_VIRTUALSERVER_ + 1];
};

struct PackageConnectStatus : PackageData_t
{
	UINT64 serverConnectionHandlerID;
	TSConnectStatus ConnectionState;
};




#pragma pack(pop)