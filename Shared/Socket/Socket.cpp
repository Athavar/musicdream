// Ts3BotServerSocket.cpp: Definiert die exportierten Funktionen für die DLL-Anwendung.
//

#include "stdafx.h"
#include "Socket.h"

bool Socket::progamm_running_ = true;
int Socket::nofSockets_ = 0;
int Socket::nextid_ = 0;
std::map<uint8_t, Socket *>* Socket::client_list = new std::map<uint8_t, Socket *>();
std::queue<QueuePackage_t>* Socket::ReciveQueue = new std::queue<QueuePackage_t>();
std::mutex Socket::ReciveQueueMutex;
std::condition_variable Socket::NewReciveData;

void Socket::Start()
{
	if (!nofSockets_)
	{
		WSADATA info;
		if (WSAStartup(MAKEWORD(2, 0), &info))
		{
			throw "Could not start WSA";
		}
	}
	++nofSockets_;
}

void Socket::End()
{
	WSACleanup();
}

Socket::Socket() : s_(0)
{
	Start();
	// UDP: use SOCK_DGRAM instead of SOCK_STREAM
	Create();

	refCounter_ = new int(1);
}

Socket::Socket(SOCKET s) : s_(s)
{
	Start();
	refCounter_ = new int(1);
	this->id = nextid_++;
	Socket::client_list->emplace(id, this);
	StartReceiveHandler();
};

Socket::~Socket()
{
	if (!--(*refCounter_))
	{
		Close();
		delete refCounter_;
	}
	if (Socket::client_list->find(id) != Socket::client_list->end()) Socket::client_list->erase(id);

	--nofSockets_;
	if (!nofSockets_) End();
}

Socket::Socket(const Socket& o)
{
	refCounter_ = o.refCounter_;
	(*refCounter_)++;
	s_ = o.s_;

	nofSockets_++;
}

Socket& Socket::operator=(Socket& o)
{
	(*o.refCounter_)++;

	refCounter_ = o.refCounter_;
	s_ = o.s_;

	nofSockets_++;

	return *this;
}

void Socket::Create()
{
	s_ = socket(AF_INET, SOCK_STREAM, 0);

	if (s_ == INVALID_SOCKET)
	{
		throw "INVALID_SOCKET";
	}
}

void Socket::Close()
{
	closesocket(s_);
}

void Socket::CloseEnd()
{
	Close();
	if (reciveThread->joinable()) reciveThread->join();
}

void Socket::StartReceiveHandler()
{
	reciveThread = new std::thread(&Socket::ConnectHandler, this);
}

void Socket::ConnectHandler()
{
	if(SocketConnectHandler != nullptr)	//is a ClientSocket on the Server
		SocketConnectHandler(this->id);

	PackageHeader_t header = { EMPTY, 0 }; //[HEADERSIZE];
	char buf[BUFFERLENGTH];
	char buf2[BUFFERLENGTH2];
	int buf2offset = 0;
	int processedBytes;
	int remainingbyte = 0;
	int cpbyte;
	unsigned char * data;

	while (progamm_running_)
	{
		processedBytes = 0;
		int rv = recv(s_, buf, BUFFERLENGTH, 0);
		if (rv <= 0) break;

		if (remainingbyte != 0)
		{
			cpbyte = remainingbyte < rv ? remainingbyte : rv;
			memmove(buf2 + buf2offset, buf, cpbyte);
			processedBytes += cpbyte;
			buf2offset += cpbyte;
			remainingbyte -= cpbyte;
			if (remainingbyte <= 0)
			{
				if (header._PID == EMPTY)
				{
					memmove(&header, buf2, buf2offset);
				}
				else
				{
					data = new unsigned char[buf2offset];
					memmove(data, buf2, buf2offset);
					std::unique_lock<std::mutex> lck(ReciveQueueMutex);
					ReciveQueue->emplace(std::make_pair(this->id, data));
					lck.unlock();
					NewReciveData.notify_one();
					header._PID = EMPTY;
				}
				buf2offset = 0;
			}
		}
		while ((rv - processedBytes) > 0)
		{
			if (header._PID == EMPTY)
			{
				if ((rv - processedBytes) >= HEADERSIZE)
				{ //complete Header are received
					memcpy(&header, buf + processedBytes, HEADERSIZE);
					processedBytes += HEADERSIZE;
				}
				else
				{//not complete Header are received
					int diff = rv - processedBytes;
					remainingbyte += HEADERSIZE - diff;
					memcpy(buf2, buf + processedBytes, diff);
					buf2offset += diff;
					processedBytes += diff;
				}
			}
			else
			{
				if ((rv - processedBytes) >= header.length)
				{ //all bytes are received
					data = new unsigned char[header.length];

					memcpy(data, buf + processedBytes, header.length);
					std::unique_lock<std::mutex> lck(ReciveQueueMutex);
					ReciveQueue->emplace(this->id, data);
					lck.unlock();
					NewReciveData.notify_one();
					processedBytes += header.length;
					header._PID = EMPTY;
				}
				else
				{ //not all bytes are received
					int diff = rv - processedBytes;
					remainingbyte += header.length - diff;
					memcpy(buf2, buf + processedBytes, diff);
					buf2offset += diff;
					processedBytes += diff;
				}
			}
		}
	}
	if (SocketDisconnectHandler != nullptr)	//is a ClientSocket on the Server
	{
		SocketDisconnectHandler(this->id);
	}

	if ( !isSocketClient && progamm_running_) //ist no normal exit -> disconnect
	{
		delete this;
	}
	return;
}

Socket * Socket::getSocket(int id)
{
	if (Socket::client_list->size() == 0) return nullptr;
	if (id == -1) return Socket::client_list->begin()->second;
	return (* Socket::client_list)[id];
}

void Socket::CloseAllSockets()
{
	progamm_running_ = false;
	std::map<uint8_t, Socket *>::iterator itr = client_list->begin();
	while (itr != client_list->end()) {
		Socket * s = itr->second;
		s->CloseEnd();
		itr = client_list->erase(itr);
		delete s;
	}
}

void Socket::SendPackage(PackageData_t *package, int length)
{
	char * data = new char[HEADERSIZE + length];
	PackageHeader_t header = { package->_PID, (uint16_t)length };
	memcpy(data, &header, HEADERSIZE);
	memcpy(data + HEADERSIZE, package, length);
	SendBytes(data, (int)(HEADERSIZE + length));
	delete[] data;
}

void Socket::SendBytes(const char * data, const int length)
{
	send(s_, data, length, 0);
}