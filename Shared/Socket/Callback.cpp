#include "stdafx.h"
#include "Callback.h"


SocketConnectCallback SocketConnectHandler = 0;
void SetConnectCallback(SocketConnectCallback handler) {SocketConnectHandler = handler;}

SocketDisconnectCallback SocketDisconnectHandler = 0;
void SetDisconnectCallback(SocketDisconnectCallback handler) {SocketDisconnectHandler = handler;}

BotConnectStateChangeCallback BotConnectStateChangeHandler = 0;
void SetBotConnectStateChangeCallback(BotConnectStateChangeCallback handler) {BotConnectStateChangeHandler = handler;}

ServerDataCallback ServerDataHandler = 0;
void SetServerDataCallback(ServerDataCallback handler) {ServerDataHandler = handler;}

ChannelDataCallback ChannelDataHandler = 0;
void SetChannelDataCallback(ChannelDataCallback handler) {ChannelDataHandler = handler;}

ClientDataCallback ClientDataHandler = 0;
void SetClientDataCallback(ClientDataCallback handler) { ClientDataHandler = handler; }

ClientActionCallback ClientActionHandler = 0;
void SetClientActionCallback(ClientActionCallback handler) { ClientActionHandler = handler; }

RevMessageCallback RevMessageHandler = 0;
void SetRevMessageCallback(RevMessageCallback handler) { RevMessageHandler = handler; }





#ifdef SELFCONNECT
TS3StartConnectionCallback TS3StartConnectionHandler = 0;
void SetTS3StartConnectionCallback(TS3StartConnectionCallback handler) {TS3StartConnectionHandler = handler;}

TS3StopConnectionCallback TS3StopConnectionHandler = 0;
void SetTS3StopConnectionCallback(TS3StopConnectionCallback handler) {TS3StopConnectionHandler = handler;}
#endif



TS3ReSendAllDataCallback TS3ReSendAllDataHandler = 0;
void SetTS3ReSendAllDataCallback(TS3ReSendAllDataCallback& handler) { TS3ReSendAllDataHandler = handler; }

TS3SendChannelTextMessageCallback TS3SendChannelTextMessageHandler = 0;
void SetTS3SendChannelTextMessageCallback(TS3SendChannelTextMessageCallback& handler) { TS3SendChannelTextMessageHandler = handler; }

TS3SendPrivateTextMessageCallback TS3SendPrivateTextMessageHandler = 0;
void SetTS3SendPrivateTextMessageCallback(TS3SendPrivateTextMessageCallback& handler) { TS3SendPrivateTextMessageHandler = handler; }

TS3ActivateAudioCallback TS3ActivateAudioHandler = 0;
void SetTS3ActivateAudioCallback(TS3ActivateAudioCallback& handler) { TS3ActivateAudioHandler = handler; }

TS3SendAudioDataCallback TS3SendAudioDataHandler = 0;
void SetTS3SendAudioDataCallback(TS3SendAudioDataCallback& handler) { TS3SendAudioDataHandler = handler; }

int GetCurrentStackSize() {
	char localVar;
	int curStackSize = (&localVar) - _topOfStack;
	if (curStackSize < 0) curStackSize = -curStackSize;  // in case the stack is growing down
	return curStackSize;
}