#pragma once
#include  "Socket.h"
#include <WinSock2.h>

class SocketClient : public Socket
{
public:
	SocketClient(const std::string& host, int port, int retryTimeout = 0);
	~SocketClient();

	void StartReceiveHandler() override;
private:
	typedef Socket super;
	sockaddr_in addr;
	int retryTimeout;
	void   ConnectHandler() override;
	void   Connect();
};