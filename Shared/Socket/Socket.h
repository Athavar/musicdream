#pragma once

#ifdef WIN32
#include <windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#pragma comment(lib, "Ws2_32.lib")
#endif


#include "Callback.h"
#include <stdio.h>
#include <stdlib.h>


#include <string>
#include <mutex>
#include <queue>
#include <map>

#include "ts3_socketinteraction.h"

enum TypeSocket { BlockingSocket, NonBlockingSocket };

typedef std::pair<uint8_t, unsigned char *> QueuePackage_t;


class Socket
{
public:

	virtual ~Socket();
	Socket(const Socket&);
	Socket& operator=(Socket&);


	void   Close();
	void   CloseEnd();

	void   SendPackage(PackageData_t * package, int length);
	void   SendBytes(const char * data, const int length);

	virtual void   StartReceiveHandler();

	static Socket * getSocket(int id);
	static void CloseAllSockets();

	static std::queue<QueuePackage_t>* ReciveQueue;
	static std::mutex ReciveQueueMutex;
	static std::condition_variable NewReciveData;

protected:
	friend class SocketClient;
	friend class SocketServer;

	virtual void   ConnectHandler();

	Socket(SOCKET s);
	Socket();
	void Create();

	SOCKET s_;

	int* refCounter_;
	bool isSocketClient = false;

private:
	uint8_t id;
	std::thread * reciveThread;
	static void Start();
	static void End();
	static int  nofSockets_;
	static int  nextid_;
	static bool progamm_running_;

	static std::map<uint8_t, Socket *>* client_list;
};

