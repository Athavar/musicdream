#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fstream>
#include <functional>
#include "ts3_socketinteraction.h"

#define SOCKETSERVERDLL_EXPORTS

#ifdef DLLEXPORT
#ifdef SOCKETSERVERDLL_EXPORTS
#define SOCKETSERVERDLL_API extern "C" __declspec(dllexport) 
#else
#define SOCKETSERVERDLL_API extern "C" __declspec(dllimport) 
#endif
#else
#define SOCKETSERVERDLL_API
#endif

typedef void(__stdcall * SocketConnectCallback)(const int socketid);
extern SocketConnectCallback SocketConnectHandler;
SOCKETSERVERDLL_API void SetConnectCallback(SocketConnectCallback handler);

typedef void(__stdcall * SocketDisconnectCallback)(const int socketid);
extern SocketDisconnectCallback SocketDisconnectHandler;
SOCKETSERVERDLL_API void SetDisconnectCallback(SocketDisconnectCallback handler);

typedef void(__stdcall * BotConnectStateChangeCallback)(const int socketid, UINT64 serverConnectionHandlerID, int state);
extern BotConnectStateChangeCallback BotConnectStateChangeHandler;
SOCKETSERVERDLL_API void SetBotConnectStateChangeCallback(BotConnectStateChangeCallback handler);

typedef void(__stdcall * ServerDataCallback)(const int socketid, UINT64 serverConnectionHandlerID, char * uniqueIdentifier, char * name, char * welcomeMessage, char * platform, char * version, int maxClients, bool codecEncriptionMode);
extern ServerDataCallback ServerDataHandler;
SOCKETSERVERDLL_API void SetServerDataCallback(ServerDataCallback handler);

typedef void(__stdcall * ChannelDataCallback)(const int socketid, UINT64 serverConnectionHandlerID, UINT64 channelID, UINT64 channelParentId, char * name, char * topic, char * description, int neededTalkPower,	int codec, int codecQuality, int codecLatencyFactor, bool codecOsUnencrypted, int maxClient, int maxFamilyClient, int Order, bool flagPermanant, bool flagSemiPermanent, bool flagDefault, bool flagPassword, bool flagSubscribed);
extern ChannelDataCallback ChannelDataHandler;
SOCKETSERVERDLL_API void SetChannelDataCallback(ChannelDataCallback handler);

typedef void(__stdcall * ClientDataCallback)(const int socketid, UINT64 serverConnectionHandlerID, UINT16 ClientID, UINT64 CurrentChannel, char * nickname, char * uniqueIdentifier, bool flagSelf);
extern ClientDataCallback ClientDataHandler;
SOCKETSERVERDLL_API void SetClientDataCallback(ClientDataCallback handler);

typedef void(__stdcall * ClientActionCallback)(const int socketid, UINT64 serverConnectionHandlerID, UINT16 ClientID, UINT64 oldChannal, UINT64 newChannel, int clientAction);
extern ClientActionCallback ClientActionHandler;
SOCKETSERVERDLL_API void SetClientActionCallback(ClientActionCallback handler);

typedef void(__stdcall * RevMessageCallback)(const int socketid, UINT64 serverConnectionHandlerID, UINT16 targetMode, UINT64 targetID, char * uniqueIdentifier, char * message);
extern RevMessageCallback RevMessageHandler;
SOCKETSERVERDLL_API void SetRevMessageCallback(RevMessageCallback handler);


#ifdef SELFCONNECT
typedef int (__stdcall * TS3StartConnectionCallback)(char * Identity, char * ipaddrress, unsigned int port, char * nickname, char ** defaultChannelArray, char * defaultChannelPassword, char * serverPassword);
extern TS3StartConnectionCallback TS3StartConnectionHandler;
SOCKETSERVERDLL_API void SetTS3StartConnectionCallback(TS3StartConnectionCallback handler);

typedef int (__stdcall * TS3StopConnectionCallback)( unsigned long long serverConnectionHandlerID, char * msg);
extern TS3StopConnectionCallback TS3StopConnectionHandler;
SOCKETSERVERDLL_API void SetTS3StopConnectionCallback(TS3StopConnectionCallback handler);
#endif



typedef std::function<void()> TS3ReSendAllDataCallback;
extern TS3ReSendAllDataCallback TS3ReSendAllDataHandler;
SOCKETSERVERDLL_API void SetTS3ReSendAllDataCallback(TS3ReSendAllDataCallback& handler);

typedef std::function<void(UINT64 serverConnectionHandlerID, char * message, UINT64 targetChannelID)> TS3SendChannelTextMessageCallback;
extern TS3SendChannelTextMessageCallback TS3SendChannelTextMessageHandler;
SOCKETSERVERDLL_API void SetTS3SendChannelTextMessageCallback(TS3SendChannelTextMessageCallback& handler);

typedef std::function<void(UINT64 serverConnectionHandlerID, char * message, UINT16 targetClientID)> TS3SendPrivateTextMessageCallback;
extern TS3SendPrivateTextMessageCallback TS3SendPrivateTextMessageHandler;
SOCKETSERVERDLL_API void SetTS3SendPrivateTextMessageCallback(TS3SendPrivateTextMessageCallback& handler);

typedef std::function<void(UINT64 serverConnectionHandlerID)> TS3ActivateAudioCallback;
extern TS3ActivateAudioCallback TS3ActivateAudioHandler;
SOCKETSERVERDLL_API void SetTS3ActivateAudioCallback(TS3ActivateAudioCallback& handler);

typedef std::function<void(short* buffer, int samples)> TS3SendAudioDataCallback;
extern TS3SendAudioDataCallback TS3SendAudioDataHandler;
SOCKETSERVERDLL_API void SetTS3SendAudioDataCallback(TS3SendAudioDataCallback& handler);



static char * _topOfStack;

int GetCurrentStackSize();