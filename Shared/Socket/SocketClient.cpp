#include "stdafx.h"
#include "SocketClient.h"

SocketClient::SocketClient(const std::string& host, int port, int retryTimeout) : Socket()
{
	isSocketClient = true;
	progamm_running_ = true;
	std::string error;

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	inet_pton(AF_INET, host.c_str(), &(addr.sin_addr));
	this->retryTimeout = retryTimeout;
	memset(&(addr.sin_zero), 0, 8);

	Connect();

	this->id = nextid_++;
	Socket::client_list->emplace(id, this);

	StartReceiveHandler();
}

SocketClient::~SocketClient()
{
}

void SocketClient::StartReceiveHandler()
{
	reciveThread = new std::thread(&SocketClient::ConnectHandler, this);
	reciveThread->join();
	delete reciveThread;
}


void SocketClient::Connect()
{
	while (progamm_running_)
	{
		if (::connect(s_, (sockaddr *)&addr, sizeof(sockaddr)))
		{
			int wsaerror = WSAGetLastError();
			if (retryTimeout > 0 && progamm_running_)
			{
				printf("connect failed: %d. Wait timeout %d ms \n" + wsaerror, retryTimeout);
				Sleep(retryTimeout);
			}
			else
			{
				printf("connect failed: %d\n" + wsaerror);
				break;
			}

		}
		else break;
	}
}

//Thread
void SocketClient::ConnectHandler()
{
	while (progamm_running_)
	{
		if (TS3ReSendAllDataHandler != nullptr)
			TS3ReSendAllDataHandler();
		super::ConnectHandler();
		Close();
		if (progamm_running_)
		{
			Create();
			Connect();
		}
	}
	delete this;
}