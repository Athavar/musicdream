#include "stdafx.h"
#include "SocketDLL.h"


#include <iostream>
#include <fstream>

bool running = false;
bool isServer = false;
SocketServer* server;
SocketClient* client;
std::thread * queueWorkerThread = nullptr;
std::thread * startClientThread = nullptr;

void _startClientThread(const std::string& host, int port);
void _queueWorkerThread();


void XMemCopyChar(char * des, const char * src)
{
	memmove(des, src, strlen(src) + 1);
}

void InitDLL(bool isserver)
{
	isServer = isserver;
	running = true;
	queueWorkerThread = new std::thread(_queueWorkerThread);
}

void StartSocketServer(int port)
{
	if (!isServer || !running ) return;
	server = new SocketServer(port);
}

bool StartSocketClient(const std::string& host, int port)
{
	if (isServer || !running ) return false;
	startClientThread = new std::thread(_startClientThread, host, port);
	return true;
}

void SendPackage(const int socketid, PackageData_t * package,  int length)
{
	if(Socket::getSocket(socketid) != nullptr)
		Socket::getSocket(socketid)->SendPackage(package, length);
}

void Stop()
{
	running = false;
	if (server != nullptr)
	{
		delete server;
		server = nullptr;
	}
	Socket::CloseAllSockets();

	std::queue<QueuePackage_t> empty;
	std::swap(*Socket::ReciveQueue, empty);
	QueuePackage_t qpr;
	while (!empty.empty())
	{
		qpr = empty.front();
		delete[] qpr.second;
		empty.pop();
	}

	Socket::NewReciveData.notify_all();
	if (startClientThread != nullptr && startClientThread->joinable())
	{
		startClientThread->join();
		delete startClientThread;
	}
	if (queueWorkerThread != nullptr && queueWorkerThread->joinable())
	{
		queueWorkerThread->join();
		delete queueWorkerThread;
	}
}

void ThrowTS3ERROR(unsigned int errorCode)
{

}

void _startClientThread(const std::string& host, int port)
{
	new SocketClient(host, port, 1000);
}

void gen_random(char *s, const int len) {
	static const char alphanum[] =
		"0123456789"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz";

	for (int i = 0; i < len; ++i) {
		s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
	}

	s[len] = 0;
}

void _queueWorkerThread()
{
	char topOfStack;
	_topOfStack = &topOfStack;
	QueuePackage_t queuedata;
	int socketid;
	unsigned char * cdata;
	PackageData_t * data;

	std::ofstream outputFile;
	srand(time(NULL));
	std::stringstream s;
	s << "D:\\StackSize" << (rand() % 100000);

	outputFile.open(s.str());


	std::unique_lock<std::mutex> lck(Socket::ReciveQueueMutex);
	while (running)
	{
		
		Socket::NewReciveData.wait(lck);
		if (Socket::ReciveQueue->empty())
		{
			break;
		}
		while (!Socket::ReciveQueue->empty())
		{
			queuedata = Socket::ReciveQueue->front();

			socketid = queuedata.first;
			cdata = queuedata.second;
			data = (PackageData_t *)cdata;

			Socket::ReciveQueue->pop();

			switch (data->_PID)
			{
#ifdef SELFCONNECT
			case PID::STARTCONNECTION:
			{
				PackageStartConnect * packageConnect = (PackageStartConnect *) data;

				if(TS3StartConnectionHandler != 0)
					TS3StartConnectionHandler(packageConnect->Identity, packageConnect->IpAddress, packageConnect->Port, packageConnect->Nickname, NULL, NULL, packageConnect->ServerPassword );
				break;
			}
			case PID::STOPCONNECTION:
			{
				PackageStopConnect * package = (PackageStopConnect *) data;
				if(TS3StopConnectionHandler != 0)
					TS3StopConnectionHandler(package->ServerConnectionHandlerID, package->ExitMsg);
				break;
			}
#endif
			case PID::CONNECTIONSTATE:
			{
				PackageConnectStatus * package = (PackageConnectStatus *) data;
				outputFile << "CONNECTIONSTATE "<< package->ConnectionState <<"|" << GetCurrentStackSize() << std::endl;
				if(BotConnectStateChangeHandler != 0)
					BotConnectStateChangeHandler(socketid, package->serverConnectionHandlerID, (int)package->ConnectionState);
				break;
			}
			case PID::SERVERDATA:
			{
				outputFile << "SERVERDATA |" << GetCurrentStackSize() << std::endl;
				PackageServerData * package = (PackageServerData *) data;
				if(ServerDataHandler != 0)
				{
					ServerDataHandler(socketid, package->serverConnectionHandlerID, package->UniqueIdentifier, package->Name, package->WelcomeMessage, package->Platform, package->Version, package->MaxClients, package->CodecEncriptionMode);
				}
				break;
			}

			case PID::CHANNELDATA:
			{
				PackageChannelData * package = (PackageChannelData *) data;
				outputFile << "CHANNELDATA " << package->ChannelId << " in " << package->ChannelParentId << "|" << GetCurrentStackSize() << std::endl;

				if (ChannelDataHandler != 0)
				{
					ChannelDataHandler(socketid, package->serverConnectionHandlerID, package->ChannelId, package->ChannelParentId, package->Name, package->Topic, package->Description, package->NeededTalkPower, package->Codec, package->CodecQuality, package->CodecLatencyFactor, package->CodecIsUnencrypted,
						package->MaxClients, package->MaxFamilyClients, package->Order, package->FlagPermanent, package->FlagSemiPermanent, package->FlagDefault, package->FlagPassword, package->FlagSubscribed);
				}
				break;
			}
			case PID::CLIENTDATA:
			{
				PackageClientData * package = (PackageClientData *)data;
				outputFile << "CLIENTDATA " << package->ClientId << " in "<< package->CurrentChannelID << "|" << GetCurrentStackSize() << std::endl;
				if (ClientDataHandler != 0)
				{
					ClientDataHandler(socketid, package->serverConnectionHandlerID, package->ClientId, package->CurrentChannelID, package->Nickname, package->UniqueIdentifier, package->FlagSelf);
				}

				break;
			}
			case PID::CLIENTACTION:
			{
				outputFile << "CLIENTACTION |" << GetCurrentStackSize() << std::endl;
				PackageClientAction * package = (PackageClientAction *)data;
				if (ClientActionHandler != 0) {
					ClientActionHandler(socketid, package->serverConnectionHandlerID, package->ClientId, package->oldChannelID, package->newChannelID, package->Action);
				}
				break;
			}
			case PID::REVMESSAGE:
			{
				outputFile << "REVMESSAGE |" << GetCurrentStackSize() << std::endl;
				PackageRevMessage * package = (PackageRevMessage *)data;
				if (RevMessageHandler != 0) {
					RevMessageHandler(socketid, package->serverConnectionHandlerID, package->MessageType, package->TargetID, package->UniqueIdentifier, package->Message);
				}
				break;
			}

			case PID::SENDMESSAGE:
			{
				outputFile << "SENDMESSAGE |" << GetCurrentStackSize() << std::endl;
				PackageSendMessage * package = (PackageSendMessage *)data;
				if (package->MessageType == MessageType::PRIVATE) {
					if (TS3SendPrivateTextMessageHandler != 0) {
						TS3SendPrivateTextMessageHandler(package->serverConnectionHandlerID, package->Message, (anyID)package->TargetID);
					}
				}
				else if (package->MessageType == MessageType::CHANNEL) {
					if (TS3SendChannelTextMessageHandler != 0) {
						TS3SendChannelTextMessageHandler(package->serverConnectionHandlerID, package->Message, package->TargetID);
					}
				}
				break;
			}
			case PID::ACTIVATEAUDIO:
			{
				outputFile << "ACTIVATEAUDIO |" << GetCurrentStackSize() << std::endl;
				PackageActivateAudio * package = (PackageActivateAudio *)data;
				if (TS3ActivateAudioHandler != 0) {
					TS3ActivateAudioHandler(package->serverConnectionHandlerID);
				}
				break;
			}

			case PID::AUDIODATA:
			{
				outputFile << "AUDIODATA |" << GetCurrentStackSize() << std::endl;
				if (TS3SendAudioDataHandler != 0) {
					int size;
					memcpy(&size, cdata + sizeof(PID), sizeof(int));
					size /= 2;
					short * buffer = new short[size];
					memcpy(buffer, cdata + sizeof(PID) + sizeof(size), size * sizeof(buffer[0]));

					TS3SendAudioDataHandler(buffer, size); //delete buffer in there
					delete[] buffer;
				}
				break;
			}

			default:
				break;
			}

			delete cdata;
			outputFile << "ENDE |" << GetCurrentStackSize() << std::endl;
			std::flush(outputFile);
		}
	}
	outputFile.close();
	return;
}

void ActivateAudio(const int socketid, UINT64 serverConnectionHandlerID) {

	PackageActivateAudio * package = new PackageActivateAudio;
	package->_PID = PID::ACTIVATEAUDIO;
	package->serverConnectionHandlerID = serverConnectionHandlerID;

	SendPackage(socketid, package, sizeof(PackageActivateAudio));
	delete package;
}
void SendAudioData(const int socketid, short * buffer, int size)
{
	char* data = new char[sizeof(PID) + sizeof(int) + size];
	int pid = PID::AUDIODATA;
	memmove(data, &pid, sizeof(PID));
	memmove(data + sizeof(PID), &size, sizeof(int));
	memmove(data + sizeof(PID) + sizeof(size), buffer, size);

	SendPackage(socketid, (PackageData_t*)data, sizeof(PID) + sizeof(int) + size);
	delete[] data;
}


void TS3SendMessage(const int socketid, unsigned long long serverConnectionHandlerID, int messageType, char * message, uint64 targetID) {

	PackageSendMessage * package = new PackageSendMessage;
	package->_PID = PID::SENDMESSAGE;
	package->serverConnectionHandlerID = serverConnectionHandlerID;
	package->MessageType = (MessageType)messageType;
	package->TargetID = targetID;
	XMemCopyChar(package->Message, message);
	SendPackage(socketid, package, sizeof(PackageSendMessage));
	delete package;
}



#ifdef SELFCONNECT
void StartConnection(const int socketid, char * Identity, char * ipaddress, unsigned int port, char * nickname, char * serverPassword)
{
	PackageStartConnect * package = new PackageStartConnect;
	memset(package, 0, sizeof(PackageStartConnect));


	package->_PID = PID::STARTCONNECTION;

	XMemCopyChar(package->Identity, Identity);
	XMemCopyChar(package->IpAddress, ipaddress);
	package->Port = port;
	XMemCopyChar(package->Nickname, nickname);
	XMemCopyChar(package->ServerPassword, serverPassword);

	SendPackage(socketid, package, sizeof(PackageStartConnect));
	delete package;
}

void StopConnection(const int socketid, unsigned long long serverConnectionHandlerID, char * exitmsg)
{
	PackageStopConnect * package = new PackageStopConnect;

	package->_PID = PID::STOPCONNECTION;
	package->ServerConnectionHandlerID = serverConnectionHandlerID;
	XMemCopyChar(package->ExitMsg, exitmsg);
	SendPackage(socketid, package, sizeof(PackageStopConnect));
	delete package;
}
#endif