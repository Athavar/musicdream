#pragma once

#include "stdafx.h"
#include "Socket/Callback.h"

#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <iostream>

#include "Socket/SocketServer.h"
#include "Socket/SocketClient.h"
#include "ts3_socketinteraction.h"

void XMemCopyChar(char * des, const char * src);

const static char * SOCKETSETTINGFILEPATH = "D:\\ts3bot.conf";

SOCKETSERVERDLL_API void InitDLL(bool isserver = false);

SOCKETSERVERDLL_API void StartSocketServer(int port);

SOCKETSERVERDLL_API bool StartSocketClient(const std::string& host, int port);


#ifdef SELFCONNECT
SOCKETSERVERDLL_API void StartConnection(const int socketid, char * Identity, char * ipaddress, unsigned int port, char * nickname, char * serverPassword);

SOCKETSERVERDLL_API void StopConnection(const int socketid, unsigned long long serverConnectionHandlerID, char * exitmsg);
#endif

SOCKETSERVERDLL_API void TS3SendMessage(const int socketid, unsigned long long serverConnectionHandlerID, int messageType, char * message, uint64 targetID);

SOCKETSERVERDLL_API void ActivateAudio(const int socketid, UINT64 serverConnectionHandlerID);

SOCKETSERVERDLL_API void SendAudioData(const int socketid, short * buffer, int size);

void SendPackage(const int socketid, PackageData_t * package, int length);

void ThrowTS3ERROR(unsigned int errorCode);

SOCKETSERVERDLL_API void Stop();

