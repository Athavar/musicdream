#pragma once
#include "Socket.h"

class SocketServer : public Socket {
public:
	SocketServer(int port, int connections=SOMAXCONN, TypeSocket type=BlockingSocket);
	~SocketServer();

private:
	void StartAccept();

	std::thread * accept_thread;
	Socket* Accept();
};
