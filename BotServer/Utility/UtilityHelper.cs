﻿using FFmpeg.AutoGen;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Ts3Bot.Utility
{

    class UtilityHelper
    {
       private static Random _Random = new Random(unchecked((int)Now()));

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static long Now()
        {
            return Stopwatch.GetTimestamp() / (Stopwatch.Frequency / 1000);
        }
        public static double NextRandomInt()
        {
            return _Random.Next();
        }
        public static int NextRandomInt(int maxValue)
        {
            return _Random.Next(maxValue);
        }

        public static int NextRandomInt(int minValue, int maxValue)
        {
            return _Random.Next(minValue, maxValue);
        }

        public static double NextRandomDouble()
        {
           return  _Random.NextDouble();
        }





        private struct MEMORY_BASIC_INFORMATION
        {
            public uint BaseAddress;
            public uint AllocationBase;
            public uint AllocationProtect;
            public uint RegionSize;
            public uint State;
            public uint Protect;
            public uint Type;
        }

        private const uint STACK_RESERVED_SPACE = 4096 * 16;

        [DllImport("kernel32.dll")]
        private static extern int VirtualQuery(
            IntPtr lpAddress,
            ref MEMORY_BASIC_INFORMATION lpBuffer,
            int dwLength);
        
        public unsafe static uint EstimatedRemainingStackBytes()
        {
            MEMORY_BASIC_INFORMATION stackInfo = new MEMORY_BASIC_INFORMATION();
            IntPtr currentAddr = new IntPtr((uint)&stackInfo - 4096);

            VirtualQuery(currentAddr, ref stackInfo, sizeof(MEMORY_BASIC_INFORMATION));
            return (uint)currentAddr.ToInt64() - stackInfo.AllocationBase - STACK_RESERVED_SPACE;
        }




    }

}
