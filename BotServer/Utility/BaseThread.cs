﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ts3Bot.Utility
{
    public abstract class BaseThread
    {
        ManualResetEvent _shutdownEvent = new ManualResetEvent(false);
        ManualResetEvent _pauseEvent = new ManualResetEvent(true);

        private Thread _thread;

        protected BaseThread()
        {
            _thread = new Thread(new ThreadStart(this.RunThread));
            
        }

        // Thread methods / properties
        protected void Start() => _thread.Start();
        protected void Join() => _thread.Join();
        protected bool IsAlive => _thread.IsAlive;

        protected void CheckPaused()
        {
            _pauseEvent.WaitOne(Timeout.Infinite);
        }

        protected void Pause() => _pauseEvent.Reset();

        protected void Resume() => _pauseEvent.Set();

        protected void Stop()
        {
            // Signal the shutdown event
            _shutdownEvent.Set();

            // Make sure to resume any paused threads
            _pauseEvent.Set();

            // Wait for the thread to exit
            _thread.Join();
        }
        // true no change
        // false incomming shutdown
        protected bool CheckShutdown()
        {
            return !_shutdownEvent.WaitOne(0);
        }

        public string Name { get {return _thread.Name; } set { _thread.Name = value; } }

        // Override in base class
        protected abstract void RunThread();
    }
}
