﻿using FFmpeg.AutoGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ts3Bot.Audio
{
    public sealed unsafe class AudioConvert
    {
        private SwrContext* swr_ctx;

        private readonly int src_nb_channel;
        private readonly ulong src_ch_layout;
        private readonly long src_sample_rate;
        private readonly AVSampleFormat src_sample_fmt;

        private readonly int dst_nb_channels;
        private readonly long dst_sample_rate;
        private readonly AVSampleFormat dst_sample_fmt;

        private readonly AVFrame* _pFrameOut;

        public AudioConvert(int channels, long sample_rate, AVSampleFormat sampleformat, AVCodecContext* _pCodecContext)
        {

            dst_nb_channels = channels;
            dst_sample_rate = sample_rate;
            dst_sample_fmt = sampleformat;

            src_nb_channel = _pCodecContext->channels;
            src_ch_layout = _pCodecContext->channel_layout;
            src_sample_rate = _pCodecContext->sample_rate;
            src_sample_fmt = _pCodecContext->sample_fmt;

            _pFrameOut = ffmpeg.av_frame_alloc();
            Init();
        }


        public unsafe void Dispose()
        {

            if (swr_ctx != null)
            {
                ffmpeg.swr_close(swr_ctx);
                fixed (SwrContext** sC = &swr_ctx)
                {
                    ffmpeg.swr_free(sC);
                }
            }
            if (_pFrameOut != null)
            {
                ffmpeg.av_frame_unref(_pFrameOut);
                ffmpeg.av_free(_pFrameOut);
            }
        }



        public void Init()
        {
            // resampling
            swr_ctx = ffmpeg.swr_alloc();

            ffmpeg.av_opt_set_channel_layout(swr_ctx, "in_channel_layout", (long)src_ch_layout, 0);
            ffmpeg.av_opt_set_channel_layout(swr_ctx, "out_channel_layout", ffmpeg.av_get_default_channel_layout(dst_nb_channels), 0);
            ffmpeg.av_opt_set_int(swr_ctx, "in_channel_count", src_nb_channel, 0);
            ffmpeg.av_opt_set_int(swr_ctx, "out_channel_count", dst_nb_channels, 0);
            ffmpeg.av_opt_set_int(swr_ctx, "in_sample_rate", src_sample_rate, 0);
            ffmpeg.av_opt_set_int(swr_ctx, "out_sample_rate", dst_sample_rate, 0);
            ffmpeg.av_opt_set_sample_fmt(swr_ctx, "in_sample_fmt", src_sample_fmt, 0);
            ffmpeg.av_opt_set_sample_fmt(swr_ctx, "out_sample_fmt", dst_sample_fmt, 0);

            ffmpeg.swr_init(swr_ctx).ThrowExceptionIfError();
        }



        public void Convert(AVFrame* in_frame, out AVFrame out_frame)
        {
            AVFrame debfeame = *in_frame;


            int src_nb_samples = in_frame->nb_samples;
            byte** src_data = (byte**)&in_frame->data;


            byte[] t_dst_out_data = new byte[dst_nb_channels * sizeof(byte*)];
            fixed (byte* dst_out_data = t_dst_out_data)
            {
                int dst_linesize;
                int dst_nb_samples;


                dst_nb_samples = (int)ffmpeg.av_rescale_rnd(ffmpeg.swr_get_delay(swr_ctx, src_sample_rate) + src_nb_samples, dst_sample_rate, src_sample_rate, AVRounding.AV_ROUND_UP);


                ffmpeg.av_samples_alloc(&dst_out_data,
                                                   &dst_linesize,
                                                   dst_nb_channels,
                                                   dst_nb_samples,
                                                   dst_sample_fmt, 0).ThrowExceptionIfError();
                ffmpeg.swr_convert(swr_ctx,
                                             &dst_out_data,
                                             dst_nb_samples,
                                             (byte**)&in_frame->data,
                                             src_nb_samples).ThrowExceptionIfError();

                int buffer_size = ffmpeg.av_samples_get_buffer_size(&dst_linesize,
                                                                 dst_nb_channels,
                                                                 dst_nb_samples,
                                                                 dst_sample_fmt,
                                                                 0).ThrowExceptionIfError();
                in_frame->CopyFrame(_pFrameOut);
                _pFrameOut->channels = dst_nb_channels;
                _pFrameOut->channel_layout = (ulong)ffmpeg.av_get_default_channel_layout(dst_nb_channels);
                _pFrameOut->nb_samples = dst_nb_samples;
                _pFrameOut->format = (int)dst_sample_fmt;


                ffmpeg.avcodec_fill_audio_frame(_pFrameOut,
                                             dst_nb_channels,
                                             dst_sample_fmt,
                                             dst_out_data,
                                             buffer_size,
                                             0).ThrowExceptionIfError();

                ffmpeg.av_freep(&dst_out_data);
                out_frame = *_pFrameOut;
            }
        }


    }
}
