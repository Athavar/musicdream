﻿using FFmpeg.AutoGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Ts3Bot.Audio
{
    internal static class FFmpegHelper
    {
        public static unsafe string AV_strerror(int error)
        {
            var bufferSize = 1024;
            var buffer = stackalloc byte[bufferSize];
            ffmpeg.av_strerror(error, buffer, (ulong)bufferSize);
            var message = Marshal.PtrToStringAnsi((IntPtr)buffer);
            return message;
        }

        public static int ThrowExceptionIfError(this int error)
        {
            if (error < 0)
                throw new ApplicationException(AV_strerror(error));
            return error;
        }

        public static byte[] ToByteArray(this string value)
        {
            return Encoding.ASCII.GetBytes(value);
        }

        public static byte[] ToByteArray(this long value)
        {
            return new byte[] {
        (byte) (value >> 56),
        (byte) (value >> 48),
        (byte) (value >> 40),
        (byte) (value >> 32),
        (byte) (value >> 24),
        (byte) (value >> 16),
        (byte) (value >> 8),
        (byte) value
    };
        }


        public static unsafe void CopyFrame(this AVFrame in_frame, AVFrame* out_frame)
        {

            //if(out_frame->pkt_pos != -1) ffmpeg.av_frame_unref(out_frame);
            out_frame->format = in_frame.format;
            out_frame->width = in_frame.width;
            out_frame->height = in_frame.height;
            out_frame->channels = in_frame.channels;
            out_frame->channel_layout = in_frame.channel_layout;
            out_frame->nb_samples = in_frame.nb_samples;
            //ffmpeg.av_frame_get_buffer(out_frame, 32);
            ffmpeg.av_frame_copy(out_frame, &in_frame);
            ffmpeg.av_frame_copy_props(out_frame, &in_frame);
        }



    }

}
