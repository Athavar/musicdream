﻿using FFmpeg.AutoGen;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Ts3Bot.Utility;

namespace Ts3Bot.Audio
{
    public sealed unsafe class AudioPlayer : BaseThread, IDisposable
    {
        public enum PlayControl { Play, Pause, Stop }

        public RepeatState Repeatstate { get; set; }
        public PlayControl Playcontrol { get; private set; }

        public delegate void OnTrackEndDelegate();
        public delegate void OnNewFrameDataDelegate(short[] buffer);

        private event OnTrackEndDelegate _OnTrackEnd;
        private event OnNewFrameDataDelegate _OnNewFrameData;

        public event OnTrackEndDelegate TrackEnd
        {
            add { _OnTrackEnd += value; }
            remove { _OnTrackEnd -= value; }
        }
        public event OnNewFrameDataDelegate NewFrameData
        {
            add { _OnNewFrameData += value; }
            remove { _OnNewFrameData -= value; }
        }

        private AudioTrack audioTrack;

        private AudioStreamReader audioStreamReader;

        private AVFrame* _pframe;

        private long StartTimeStamp;
        private long TrackTimeStamp;
        private long TimeBaseUnitPerMilisecond;

        private readonly AudioPlayList _QueuePlayList;

        private readonly AudioPlayList _PlayedPlayList;


        private AudioPlayer()
        {
            //Thread Name
            Name = "AudioPlayer";
        }
        public AudioPlayer(int channels, long sample_rate, AVSampleFormat sampleformat) : this()
        {
            _QueuePlayList = new AudioPlayList();

            _pframe = ffmpeg.av_frame_alloc();

            dst_nb_channels = channels;
            dst_sample_rate = sample_rate;
            dst_sample_fmt = sampleformat;
            base.Pause();
            Start();
        }

        private readonly int dst_nb_channels;
        private readonly long dst_sample_rate;
        private readonly AVSampleFormat dst_sample_fmt;


        public unsafe void Dispose()
        {
            base.Stop();
            if (_pframe != null)
            {
                ffmpeg.av_frame_unref(_pframe);
                ffmpeg.av_free(_pframe);
            }
        }

        public void AddToQueue(AudioTrack audioTrack, bool forcePlay = false)
        {
            _QueuePlayList.AddTrack(audioTrack, 0);
        }

        #region player controller

        public bool Play()
        {
            if (Playcontrol == PlayControl.Play) return true;

            if (audioStreamReader == null && !NextTrack()) return false;

            Playcontrol = PlayControl.Play;
            base.Resume();
            return true;
        }

        public new void Pause()
        {
            Playcontrol = PlayControl.Pause;
        }
        public new void Stop()
        {
            Playcontrol = PlayControl.Stop;
            TrackTimeStamp = 0;
        }

        #endregion

        #region frame handeling

        public int nextframeRecursiver = 0;

        private bool NextTrack()
        {
            if (_PlayedPlayList != null && _PlayedPlayList.TrackCount != 0 && (_PlayedPlayList.SelectNextTrack() || Repeatstate == RepeatState.AllRepeat))
            {
                if (_PlayedPlayList.SelectedTrack == null) _PlayedPlayList.SelectNextTrack();
                audioTrack = _PlayedPlayList.SelectedTrack;
            }
            else
            {
                if (!_QueuePlayList.SelectNextTrack()) return false;
                _QueuePlayList.RemoveTrack(0);
                audioTrack = _QueuePlayList.SelectedTrack;
            }

            audioStreamReader = new AudioStreamReader(audioTrack.URL, dst_nb_channels, dst_sample_rate, dst_sample_fmt);
            TimeBaseUnitPerMilisecond = audioStreamReader.TimeBaseUnitPerSecond / 1000;
            TrackTimeStamp = 0;
            return true;
        }

        private unsafe bool NextFrame()
        {
            if (nextframeRecursiver == 3) return false;
            nextframeRecursiver++;
            try
            {

                if (audioStreamReader.TryDecodeNextFrame(out *_pframe))
                {
                    nextframeRecursiver = 0;
                    return true;
                }
            }
            catch (ApplicationException e)
            {
                Console.WriteLine(e.ToString());
                //return NextFrame();
                throw e;

            }
            _OnTrackEnd?.Invoke();
            nextframeRecursiver = 0;
            StartTimeStamp = UtilityHelper.Now();
            if (Repeatstate == RepeatState.SingleRepeat)
            {
                audioStreamReader.CurrentTime = 0;
            }
            else
            {
                nextframeRecursiver = 0;
                if (!NextTrack()) return false;
            }
            return NextFrame();
        }

        protected override void RunThread()
        {
            Thread.CurrentThread.Priority = ThreadPriority.Highest;
            base.CheckPaused();


            short[] dst_data = new short[0];

            long diff;
            float volume = 0.1f;
            while (CheckShutdown())
            {
                StartTimeStamp = UtilityHelper.Now() - TrackTimeStamp;
                audioStreamReader.CurrentTime = TrackTimeStamp;
                while (CheckShutdown() && Playcontrol == PlayControl.Play && NextFrame())
                {
                    TrackTimeStamp = _pframe->best_effort_timestamp / TimeBaseUnitPerMilisecond;

                    try
                    {
                        if (!Convert.ToBoolean(_pframe->interlaced_frame))
                        {
                            dst_data = new short[_pframe->linesize[0] / sizeof(short)];

                            short* sdata = (short*)_pframe->extended_data[0];
                            for (int i = 0; i < _pframe->linesize[0] / sizeof(short); i++)
                            {
                                dst_data[i] = (short)(sdata[i] * volume);
                            }
                        }
                        else
                        {
                            short temp;
                            dst_data = new short[(_pframe->linesize[0] / 2) * _pframe->channels];

                            fixed (short* data = &dst_data[0])
                            {
                                for (int channel = 0; channel < _pframe->channels; channel++)
                                {
                                    short* sdata = (short*)_pframe->extended_data[channel];
                                    for (int i = 0; i < _pframe->linesize[0] / sizeof(short); i++)
                                    {
                                        temp = _pframe->data[(uint)channel][i];
                                        data[(channel + 1) + (i * 2)] = temp;
                                    }
                                }

                            }


                        }

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    
                    diff = (StartTimeStamp + TrackTimeStamp) - UtilityHelper.Now();
                    if (diff > 0)
                        Thread.Sleep((int)diff);

                    _OnNewFrameData?.Invoke(dst_data);
                }
                if (Playcontrol == PlayControl.Play) Playcontrol = PlayControl.Stop;
                base.Pause();
                base.CheckPaused();

            }


        }
        #endregion

    }
}