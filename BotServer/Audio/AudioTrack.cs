﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ts3Bot.Audio
{
    public enum RepeatState { NoneRepeat, SingleRepeat, AllRepeat }

    public class AudioTrack
    {
        public string URL { get; private set; }
        public string FileName { get; private set; }
        public string Title { get; private set; }
        public string Artist { get; private set; }
        public string Album { get; private set; }
        public string TrackNumber { get; private set; }
        public string Genre { get; private set; }
        public string Comment { get; private set; }

        public AudioTrack(string url)
        {
            FileName = Path.GetFileName(url);
            URL = url;

            AudioInfo asr = new AudioInfo(url);
            foreach (var kv in asr.GetContextInfo())
            {
                switch (kv.Key)
                {
                    case "title":
                        Title = kv.Value;
                        break;
                    case "artist":
                        Artist = kv.Value;
                        break;
                    case "album":
                        Album = kv.Value;
                        break;
                    case "track":
                        TrackNumber = kv.Value;
                        break;
                    case "genre":
                        Genre = kv.Value;
                        break;
                    case "comment":
                        Comment = kv.Value;
                        break;
                }
            }
        }
    }
}
