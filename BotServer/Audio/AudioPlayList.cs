﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ts3Bot.Utility;

namespace Ts3Bot.Audio
{

    public class AudioPlayList
    {

        public enum PlayListSelectMode { NextItem, RandomItem }

        private List<AudioTrack> _Tracks;


        public AudioPlayList()
        {
            _Tracks = new List<AudioTrack>();
            SelectMode = PlayListSelectMode.NextItem;
            SelectedTrack = null;
        }


        public void AddTrack(AudioTrack track)
        {
            _Tracks.Add(track);
        }
        public void AddTrack(AudioTrack track, int index)
        {
            _Tracks.Insert(index, track);
        }
        public void RemoveTrack(int index)
        {
            _Tracks.RemoveAt(index);
        }


        public PlayListSelectMode SelectMode { get; set; }

        public int TrackCount { get { return _Tracks.Count; } }

        public AudioTrack SelectedTrack { get; private set; }

        public bool SelectNextTrack()
        {
            int nextindex;
            if (_Tracks.Count != 0)
            {

                if (SelectMode == PlayListSelectMode.RandomItem)
                {
                    do
                    {
                        nextindex = UtilityHelper.NextRandomInt(0, TrackCount);
                    }
                    while (SelectedTrack == _Tracks[nextindex]);
                    SelectedTrack = _Tracks[nextindex];
                }
                if (SelectMode == PlayListSelectMode.NextItem)
                {
                    if (SelectedTrack == null)
                    {
                        SelectedTrack = _Tracks.First();
                    }
                    else
                    {
                        nextindex = _Tracks.IndexOf(SelectedTrack) + 1;
                        if (nextindex < _Tracks.Count) SelectedTrack = _Tracks[nextindex];
                        else SelectedTrack = null;
                    }
                }
            }
            return SelectedTrack != null;
        }

    }
}
