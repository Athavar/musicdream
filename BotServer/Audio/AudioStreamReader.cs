﻿using FFmpeg.AutoGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Ts3Bot.Utility;


namespace Ts3Bot.Audio
{
    public sealed unsafe class AudioStreamReader : AudioInfo, IDisposable
    {
        private AVCodecContext* _pCodecContext;
        private readonly int _streamIndex;
        private readonly AVFrame* _pFrame;
        private readonly AVFrame* _pFrameOut;
        private readonly AVPacket* _pPacket;

        private readonly bool needconvert = false;
        readonly AudioConvert ac = null;
        readonly AudioFilter af = null;
        
        public AudioStreamReader(string url) :  base(url)
        {
            ffmpeg.avformat_find_stream_info(_pFormatContext, null).ThrowExceptionIfError();
            AVCodec* codec;
            AVStream* pStream = null;

            for (var i = 0; i < _pFormatContext->nb_streams; i++)
                if (_pFormatContext->streams[i]->codec->codec_type == AVMediaType.AVMEDIA_TYPE_AUDIO)
                {
                    pStream = _pFormatContext->streams[i];
                    break;
                }

            if (pStream == null) throw new InvalidOperationException("Could not found audio stream.");

            _streamIndex = pStream->index;
            _pCodecContext = pStream->codec;

            var codecId = _pCodecContext->codec_id;
            var pCodec = ffmpeg.avcodec_find_decoder(codecId);
            if (pCodec == null) throw new InvalidOperationException("Unsupported codec.");

            ffmpeg.avcodec_open2(_pCodecContext, pCodec, null).ThrowExceptionIfError();

            CodecName = ffmpeg.avcodec_get_name(_pCodecContext->codec_id);
            SampleRate = _pCodecContext->sample_rate;
            MaxFrameNumber = (int)Math.Round(((double)(pStream->duration) / (pStream->time_base.den / pStream->time_base.num)) * SampleRate / _pCodecContext->frame_size) - 1;
            Duration = pStream->duration / ((pStream->time_base.den / pStream->time_base.num) / 1000);

            _pPacket = ffmpeg.av_packet_alloc();
            _pFrame = ffmpeg.av_frame_alloc();
            _pFrameOut = ffmpeg.av_frame_alloc();
        }

        public AudioStreamReader(string url, int channels, long sample_rate, AVSampleFormat sampleformat) : this(url)
        {
            if(_pCodecContext->sample_rate != sample_rate 
                || _pCodecContext->channels != channels 
                || _pCodecContext->sample_fmt != sampleformat)
            {
                needconvert = true;
                ac = new AudioConvert(channels, sample_rate, sampleformat, _pCodecContext);
            }

            //af = new AudioFilter(_pCodecContext, channels, sample_rate, sampleformat);
        }

        public unsafe void Dispose()
        {
            if (_pFrame != null)
            {
                ffmpeg.av_frame_unref(_pFrame);
                ffmpeg.av_free(_pFrame);
            }
            if (_pFrameOut != null)
            {
                ffmpeg.av_frame_unref(_pFrameOut);
                ffmpeg.av_free(_pFrameOut);
            }

            if (_pPacket != null)
            {
                ffmpeg.av_packet_unref(_pPacket);
                ffmpeg.av_free(_pPacket);
            }
            if(_pCodecContext != null)
            {

                ffmpeg.avcodec_close(_pCodecContext);

                fixed (AVCodecContext** cc = &_pCodecContext)
                {
                    ffmpeg.avcodec_free_context(cc);
                }
            }
            
            fixed (AVFormatContext** fc = &_pFormatContext)
            {
                ffmpeg.avformat_close_input(fc);
                ffmpeg.av_free(fc);
            }
        }
        
        

        public string CodecName { get; }

        public int SampleRate { get; }

        //Duration in milliseconds
        public long Duration { get; }

        
        public int MaxFrameNumber { get; }

        public double CurrentTime {
            get {
                return _pFrame->best_effort_timestamp / TimeBaseUnitPerSecond;
            } set {
                if (value > Duration)
                    ffmpeg.av_seek_frame(_pFormatContext, _streamIndex, (long)(Duration * TimeBaseUnitPerSecond), 0);
                else
                    ffmpeg.av_seek_frame(_pFormatContext, _streamIndex, (long)(value * TimeBaseUnitPerSecond), 0);
            }
        }

        public int TimeBaseUnitPerSecond { get {
                return _pCodecContext->pkt_timebase.den / _pCodecContext->pkt_timebase.num;
            } }



        public bool TryDecodeNextFrame(out AVFrame frame)
        {
            ffmpeg.av_frame_unref(_pFrame);
            int error;
            do
            {
                try
                {
                    do
                    {
                        error = ffmpeg.av_read_frame(_pFormatContext, _pPacket);
                        if (error == ffmpeg.AVERROR_EOF)
                        {
                            frame = *_pFrame;
                            return false;
                        }

                        error.ThrowExceptionIfError();
                    } while (_pPacket->stream_index != _streamIndex);

                    ffmpeg.avcodec_send_packet(_pCodecContext, _pPacket).ThrowExceptionIfError();
                }
                finally
                {
                    ffmpeg.av_packet_unref(_pPacket);
                }

                error = ffmpeg.avcodec_receive_frame(_pCodecContext, _pFrame);
            } while (error == ffmpeg.AVERROR(ffmpeg.EAGAIN));

            error.ThrowExceptionIfError();
            if (!needconvert)
            {
                _pFrame->CopyFrame(_pFrameOut);
            }
            else
            {
                ac.Convert(_pFrame, out *_pFrameOut);
            }
            if (af != null)
            {
                af.Filter(_pFrameOut, out frame);
            }else
            {
                frame = *_pFrameOut;
            }
            return true;
        }
    }
}
