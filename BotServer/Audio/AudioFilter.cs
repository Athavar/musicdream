﻿using FFmpeg.AutoGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ts3Bot.Audio
{
    public sealed unsafe class AudioFilter : IDisposable
    {
        private AVFilterGraph* m_filterGraph = null;
        private AVFilterContext* m_aBufferSourceFilterContext = null;
        private AVFilterContext* m_aBufferSinkFilterContext = null;

        private float Volume { get; set; }


        private readonly int dst_nb_channels;
        private readonly long dst_sample_rate;
        private readonly AVSampleFormat dst_sample_fmt;

        private readonly AVFrame* _pFrameOut;
        
        public AudioFilter(AVCodecContext* codec_ctx, int channels, long sample_rate, AVSampleFormat sampleformat)
        {
            dst_nb_channels = channels;
            dst_sample_rate = sample_rate;
            dst_sample_fmt = sampleformat;
            _pFrameOut = ffmpeg.av_frame_alloc();
            Init(codec_ctx);
        }

        public void Dispose()
        {
            Cleanup(m_filterGraph);
            if (_pFrameOut != null)
            {
                ffmpeg.av_frame_unref(_pFrameOut);
                ffmpeg.av_free(_pFrameOut);
            }
        }

        public bool Init(AVCodecContext* codec_ctx)
        {

            m_filterGraph = GetAllocatedFilterGraph();


            string aBufferFilterArguments = string.Format("sample_fmt={0}:channel_layout=0x{1}:sample_rate={2}:time_base={3}/{4}",
                (int)dst_sample_fmt,
                ffmpeg.av_get_default_channel_layout(dst_nb_channels),
                dst_sample_rate,
                codec_ctx->time_base.num,
                codec_ctx->time_base.den);

            AVFilterContext* aBufferSourceFilterContext = CreateFilter("abuffer", m_filterGraph, aBufferFilterArguments);
            AVFilterContext* volumeFilterContext = CreateFilter("volume", m_filterGraph, string.Format("volume={0}", Volume));
            AVFilterContext* aBufferSinkFilterContext = CreateFilter("abuffersink", m_filterGraph);

            LinkFilter(aBufferSourceFilterContext, volumeFilterContext);
            LinkFilter(volumeFilterContext, aBufferSinkFilterContext);

            SetFilterGraphConfiguration(m_filterGraph, null);

            m_aBufferSourceFilterContext = aBufferSourceFilterContext;
            m_aBufferSinkFilterContext = aBufferSinkFilterContext;

            return true;
        }

        private readonly int AV_BUFFERSRC_FLAG_KEEP_REF = 8;
        public void Filter(AVFrame* in_frame, out AVFrame out_frame)
        {
            if (m_aBufferSourceFilterContext != null && m_aBufferSinkFilterContext != null)
            {
                ffmpeg.av_buffersrc_add_frame_flags(m_aBufferSourceFilterContext, in_frame, AV_BUFFERSRC_FLAG_KEEP_REF).ThrowExceptionIfError();
                
                ffmpeg.av_buffersink_get_frame(m_aBufferSinkFilterContext, _pFrameOut).ThrowExceptionIfError();
            }
            out_frame = *_pFrameOut;
           //ffmpeg.avfilter_graph_get_filter(m_filterGraph, "volume")->;
        }

        #region Private Cleanup Helper Functions

        private static void Cleanup(AVFilterGraph* filterGraph)
        {
            if (filterGraph != null)
            {
                ffmpeg.avfilter_graph_free(&filterGraph);
            }
        }

        #endregion

        #region Provate Helpers

        private AVFilterGraph* GetAllocatedFilterGraph()
        {
            AVFilterGraph* filterGraph = ffmpeg.avfilter_graph_alloc();
            if (filterGraph == null)
            {
            }
            return filterGraph;
        }

        private AVFilter* GetFilterByName(string name)
        {
            AVFilter* filter = ffmpeg.avfilter_get_by_name(name);
            if (filter == null)
            {
                Console.Error.Write($"Could not find the {name} filter.\n");
            }

            return filter;
        }

        private void SetFilterGraphConfiguration(AVFilterGraph* filterGraph, void* logContext)
        {
            ffmpeg.avfilter_graph_config(filterGraph, logContext).ThrowExceptionIfError();
 
        }

        private AVFilterContext* CreateFilter(string filterName, AVFilterGraph* filterGraph, string filterArguments = null)
        {
            AVFilter* filter = GetFilterByName(filterName);
            AVFilterContext* filterContext;

            ffmpeg.avfilter_graph_create_filter(&filterContext, filter, filterName, filterArguments, null, filterGraph).ThrowExceptionIfError();

            return filterContext;
        }

        private void LinkFilter(AVFilterContext* source, AVFilterContext* destination)
        {
            ffmpeg.avfilter_link(source, 0, destination, 0).ThrowExceptionIfError();
        }

        #endregion



    }
}
