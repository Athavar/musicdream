﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ts3Bot.Configuration
{
    public struct TS3BotData
    {
        public string Name;            //32
        public string IpAddress;        //255
        public ushort Port;            //2
        public string Identity;        //28
        public string Nickname;        //28
        public string serverPassword;  //40
        public string ExitMsg;  //80
    }

    class BotDatabase
    {

        #region Instance

        public static BotDatabase Instance { get { return Nested.instance; } }

        private class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }

            internal static readonly BotDatabase instance = new BotDatabase();
        }

        #endregion

        private const string FILENAME = "Bot.db";

        private SQLiteConnection m_dbConnection;


        public BotDatabase()
        {
        }

        ~BotDatabase()
        {
            if (m_dbConnection != null && (m_dbConnection.State != System.Data.ConnectionState.Broken || m_dbConnection.State != System.Data.ConnectionState.Broken)) m_dbConnection.Close();
        }

        public void Init()
        {
            if (!File.Exists(FILENAME))
            {
                SQLiteConnection.CreateFile(FILENAME);
                m_dbConnection = new SQLiteConnection(string.Format("Data Source={0};Version=3;", FILENAME));
                m_dbConnection.Open();
                SQLiteCommand sqlite_cmd = m_dbConnection.CreateCommand();
                sqlite_cmd.CommandText = "CREATE TABLE Ts3Bot (name TEXT PRIMARY KEY, ipaddress TEXT NOT NULL, port INTEGER NOT NULL, nickname TEXT NOT NULL, serverpassword TEXT NOT NULL, identity TEXT  NOT NULL)";
                sqlite_cmd.ExecuteNonQuery();
            } else
            {
                m_dbConnection = new SQLiteConnection(string.Format("Data Source={0};Version=3;", FILENAME));
                m_dbConnection.Open();
            }

#if SELFCONNECT
            _botname = GetTS3BotNames();
#endif
        }

#if SELFCONNECT
        private IList<string> _botname;

        private string _defaultIdentity = "11VyxJK0b4XMslYGXE7lv/2u+03PR9CKWUHQQ0KZTMadF4kcHtZfQZgMi5XBx1yIGFeVFtkRglcWjwUAQAie2l0AQN8YGBRVHJWAlYGK1kICSx2ZmdWHkB5SVg3eHF9W1RpFQdjA3AvRkxYd0loQUlsZG8zZlc4Qk5ZS0hIYWppc3h0dEtYbnN1RXBTaXpLbXRIQTJKdFJFWTY=";
        
        private string _defaultExitMsg = "Bot to Exit";  

        private IList<string> GetTS3BotNames()
        {
            SQLiteCommand sqlite_cmd = m_dbConnection.CreateCommand();
            sqlite_cmd.CommandText = "SELECT `name` FROM Ts3Bot";
            var reader = sqlite_cmd.ExecuteReader();
            var list = new List<string>();
            while (reader.Read())
            {
                list.Add(reader.GetString(reader.GetOrdinal("name")));
            }
            return list;
        }
        public TS3BotData GetTs3BotData(string name)
        {
            if (!_botname.Contains(name)) return new TS3BotData { };

            SQLiteCommand sqlite_cmd = m_dbConnection.CreateCommand();
            sqlite_cmd.CommandText = $"SELECT `ipaddress`, `port`, `nickname`, `serverpassword`, `identity` FROM Ts3Bot WHERE `name` = '{name}'";
            var reader = sqlite_cmd.ExecuteReader();
            if (reader.Read())
            {
                return new TS3BotData {
                    Name = name,
                    IpAddress = reader.GetString(reader.GetOrdinal("ipaddress")),
                    Port = ushort.Parse(reader.GetInt64(reader.GetOrdinal("port")) + ""),
                    Nickname = reader.GetString(reader.GetOrdinal("nickname")),
                    serverPassword = reader.GetString(reader.GetOrdinal("serverpassword")),
                    Identity = reader.GetString(reader.GetOrdinal("identity")),
                    ExitMsg = _defaultExitMsg
                };
            }
            return new TS3BotData { };
        }

        public bool HasTS3Bot(string name)
        {
            return _botname.Contains(name);
        }

        public int AddTS3Bot(string name, string ipaddress, ushort port, string nickname)
        {
            name = name.ToLower();
            ipaddress = ipaddress.ToLower();
            if (_botname.Contains(name)) return -1;

            SQLiteCommand sqlite_cmd = m_dbConnection.CreateCommand();
            sqlite_cmd.CommandText = $"INSERT INTO Ts3Bot (name, ipaddress, port, nickname, serverpassword, identity) VALUES ('{name}', '{ipaddress}', {port.ToString()}, '{nickname}', '', '{_defaultIdentity}')";
            int res = sqlite_cmd.ExecuteNonQuery();
            if (res == 1) _botname.Add(name);

            return res;
        }

        public int SetTS3BotIdentity(string name, string identity)
        {
            if (!_botname.Contains(name)) return -1;

            SQLiteCommand sqlite_cmd = m_dbConnection.CreateCommand();
            sqlite_cmd.CommandText = $"UPDATE Ts3Bot SET `identity` = '{identity}' WHERE `name` = '{name}'";
            return sqlite_cmd.ExecuteNonQuery();
        }
#endif


    }
}
