﻿using FFmpeg.AutoGen;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Ts3Bot;
using Ts3Bot.Audio;
using Ts3Bot.Configuration;
using Ts3Bot.Network;
using Ts3Bot.Utility;

namespace Ts3Bot
{

    class MainServerBot
    {
        #region Client

        private Dictionary<Guid, IClientHandler> _clientHandler;
        public Dictionary<Guid, IClientHandler> ClientHandler
        {
            get { return _clientHandler; }

        }
        public void AddClientHandler(IClientHandler clientHandler)
        {
            _clientHandler.Add(clientHandler.GUID, clientHandler);
        }
        public void RemoveClientHandler(Guid guid)
        {
            if (_clientHandler.ContainsKey(guid))
            {
                IClientHandler clientHandler = _clientHandler[guid];
                _clientHandler.Remove(guid);
            }
            else
            {

            }
        }

        public IEnumerable<TS3ClientHandler> GetTs3Bots()
        {
            return _clientHandler.Where(ch => ch.Value is TS3ClientHandler).Select(ch => ch.Value).Cast<TS3ClientHandler>();
        }
        public TS3ClientHandler GetTs3Bots(int socketid)
        {
            return GetTs3Bots().Where(ts3 => ts3.SocketId == socketid).First();
        }







        #endregion

        #region Instance

        public static MainServerBot Instance { get { return Nested.instance; } }

        private class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }

            internal static readonly MainServerBot instance = new MainServerBot();
        }

        #endregion

        private MainServerBot()
        {
            _clientHandler = new Dictionary<Guid, IClientHandler>();
            TS3BotServerWrapper.Connect += OnClientConnect;
            TS3BotServerWrapper.Disconnect += OnClientDisconnect;
            TS3BotServerWrapper.Message += OnMessage;

        }

        private void Exit()
        {
            Console.WriteLine("Disconnect Clients");
            foreach (var client in _clientHandler)
            {
                RemoveClientHandler(client.Key);
            }
        }


        #region event

        //Exit of Programm
        public void OnTermination(object sender, ConsoleCancelEventArgs e)
        {

            TS3BotServerWrapper.Exit();
            MainServerBot.Instance.Exit();

        }

        private void OnClientDisconnect(Guid handlerGuid)
        {
            RemoveClientHandler(handlerGuid);
            Console.WriteLine(" >> " + "Client id:" + Convert.ToString(handlerGuid) + " stoped!");
        }

        private void OnClientConnect(IClientHandler clientHandler)
        {
            Console.WriteLine(" >> " + "Client id:" + Convert.ToString(clientHandler.GUID) + " started!");
        }


        static readonly char ESCAPECHAR = '!';

        private void OnMessage(IClientHandler clientHandler, UInt64 connectID, ClientType clientType, MessageType messageType, string senderId, string message)
        {
            //Vars
            var nickname = "User";
            var sendMessage = "";

            //custom def of vars
            if(clientType == ClientType.TS3)
            {
                var server = (clientHandler as TS3ClientHandler).GetServer(connectID);
                nickname = server.GetClient(senderId).Nickname;
            }
            if(message[0] == ESCAPECHAR)
            {
                var command = Regex.Replace(message.Substring(1, message.Length - 1), "\\s+", " ").Split(' ');
                switch (command[0])
                {
                    case "help":
                    case "":
                        sendMessage = "Für dich ist jede hilfe zu spät";
                        SendMessage(clientHandler, connectID, MessageType.PRIVATE, senderId, sendMessage);
                        break;
                    case "play":
                        clientHandler.AudioPlayer.Play();
                        break;
                    case "pause":
                    case "break":
                        clientHandler.AudioPlayer.Pause();
                        break;
                    case "stop":
                        clientHandler.AudioPlayer.Stop();
                        break;
                }


            } else
                SendMessage(clientHandler, connectID, MessageType.CHANNEL, senderId, string.Format("\"{0}\" schrieb{1}: {2}", nickname, messageType == MessageType.PRIVATE ? " Private" : "", message));
        }

        #endregion


        private void SendMessage(IClientHandler clientHandler, UInt64 connectID, MessageType messageType, string senderId, string message)
        {
            if(clientHandler is TS3ClientHandler)
            {
                TS3ClientHandler handler = clientHandler as TS3ClientHandler;
                var server = handler.GetServer(connectID);
                switch (messageType)
                {
                    case MessageType.CHANNEL:
                        if(UInt64.TryParse(senderId, out UInt64 channelid))
                        {
                            if (server.HasChannel(channelid))
                            {
                                server.GetChannel(channelid).SendMessage(message);
                            }
                        } else if (server.HasClient(senderId))
                        {
                            channelid = server.GetClient(senderId).CurrentChannelID;
                            if (server.HasChannel(channelid))
                            {
                                server.GetChannel(channelid).SendMessage(message);
                            }
                        }
                        break;
                    case MessageType.PRIVATE:
                        if (UInt16.TryParse(senderId, out UInt16 clientID))
                        {
                            if (server.HasClient(clientID))
                            {
                                server.GetClient(clientID).SendMessage(message);
                            }
                        } else if (server.HasClient(senderId))
                        {
                            server.GetClient(senderId).SendMessage(message);
                        }
                        break;
                }
            }
        }



        public void InitUI()
        {
            MainWindow mw = new MainWindow();
            mw.Show();
            System.Windows.Threading.Dispatcher.Run();
        }




        static void Main(string[] args)
        {
            try {
                FFmpegBinariesHelper.RegisterFFmpegBinaries();

                Console.WriteLine($"FFmpeg version info: {ffmpeg.av_version_info()}");


                /*AudioPlayer audioPlayer = new AudioPlayer();
                audioPlayer.Play(new AudioTrack("D:\\out.wav"));
                audioPlayer.Join();*/

            
                //BotDatabase.Instance.Init();
                TS3BotServerWrapper.Init(8082);
                TS3BotServerWrapper.Listen();





                Console.CancelKeyPress += MainServerBot.Instance.OnTermination;


                Thread thread = new Thread(MainServerBot.Instance.InitUI)
                {
                    Name = "UIThread",
                    IsBackground = true,
                };
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
                thread.Join();
            }
            catch (Exception e)
            {
                using (StreamWriter outputFile = new StreamWriter("D:\\errorBot.err"))
                {
                    outputFile.WriteLine(e);
                    outputFile.WriteLine(e.StackTrace);
                }
            }

        }
    }
}
