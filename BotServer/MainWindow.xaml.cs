﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Ts3Bot.Configuration;
using Ts3Bot.Network;


namespace Ts3Bot
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ConsoleContent dc = new ConsoleContent();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = dc;
            Loaded += MainWindow_Loaded;
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            InputBlock.KeyDown += InputBlock_KeyDown;
            InputBlock.Focus();
        }

        void InputBlock_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                dc.ConsoleInput = InputBlock.Text;
                dc.RunCommand();
                InputBlock.Focus();
                Scroller.ScrollToBottom();
            }
        }
    }
    public class ConsoleContent : INotifyPropertyChanged
    {
        string consoleInput = string.Empty;
        ObservableCollection<string> consoleOutput = new ObservableCollection<string>() { "Console Emulation Sample..." };

        public string ConsoleInput
        {
            get
            {
                return consoleInput;
            }
            set
            {
                consoleInput = value;
                OnPropertyChanged("ConsoleInput");
            }
        }

        public ObservableCollection<string> ConsoleOutput
        {
            get
            {
                return consoleOutput;
            }
            set
            {
                consoleOutput = value;
                OnPropertyChanged("ConsoleOutput");
            }
        }

        public void RunCommand()
        {
            ConsoleOutput.Add("<< "+ConsoleInput);
            string[] command = new Regex(@"\s+").Replace(ConsoleInput, " ").Split(' ');
            Console.ForegroundColor = ConsoleColor.Green;
            int commandSize = command.Count();
            switch (command[0].ToLower())
            {
                case "listts3":
                    {
                        foreach(var handler in MainServerBot.Instance.GetTs3Bots())
                        {

                            foreach(var server in handler.GetServer())
                            {
                                ConsoleOutput.Add(">> ==TS3Server '"+ server.Name+"' ==");
                                foreach (var channel in server.GetChannel())
                                {
                                    ConsoleOutput.Add(">> Channel: '"+channel.Name + "' " +(channel.FlagSubscribed?"Sub":"NoSub"));
                                    foreach (var client in server.GetClient().Where(client => client.CurrentChannelID == channel.ChannelID))
                                    {
                                        ConsoleOutput.Add(">> ++ " + client.Nickname);
                                    }


                                }
                            }


                        }
                        break;
                    }


                #region Disable SELFCONNECT
#if SELFCONNECT
                case "create":
                    if(commandSize >= 2)
                    {
                        switch (command[1].ToLower())
                        {
                            case "ts3":
                                if(commandSize >= 2 + 4)
                                {
                                    string name = command[2].ToLower();
                                    string ipaddress = command[3].ToLower();
                                    string nickname = command[5];
                                    if(name.Count() > 32)
                                    {
                                        ConsoleOutput.Add(string.Format(">> TS3Bot creation failed! BotName is to long!"));
                                        break;
                                    }
                                    if (name.Count() > 30)
                                    {
                                        ConsoleOutput.Add(string.Format(">> TS3Bot creation failed! Nickname is to long!"));
                                        break;
                                    }

                                    if (ipaddress.Count() > 255)
                                    {
                                        ConsoleOutput.Add(string.Format(">> TS3Bot creation failed! IpAdress/Domain is to long!"));
                                        break;
                                    }

                                   /* if (!Regex.IsMatch(ipaddress, @"((2[0-4]\d|25[0-5]|[01]?\d\d?)\\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)"))
                                    {
                                        ConsoleOutput.Add(string.Format(">> TS3Bot creation failed! IpAdress/Domain is not valid!"));
                                        break;
                                    }*/

                                    if (!ushort.TryParse(command[4], out ushort port))
                                    {
                                        ConsoleOutput.Add(string.Format(">> TS3Bot creation failed! Port is no valid number!"));
                                        break;
                                    }

                                    BotDatabase.Instance.AddTS3Bot(name, ipaddress, port, nickname);
                                    ConsoleOutput.Add(string.Format(">> TS3Bot creation success!"));
                                } else
                                {
                                    ConsoleOutput.Add(string.Format(">> TS3Bot creation need [BotName], [ipaddress/domain], [port], [nickname]"));
                                }
                                break;
                            default:
                                ConsoleOutput.Add(string.Format(">> You can only create a Bot for [ts3]"));
                                break;
                        }

                    } else
                    {
                        ConsoleOutput.Add(string.Format(">> You can create a Bot for [ts3]"));
                    }
                    break;
                case "start":
                    if (commandSize >= 2)
                    {
                        switch (command[1].ToLower())
                        {
                            case "ts3":
                                if(commandSize <= 2)
                                {
                                    ConsoleOutput.Add(string.Format(">> start TS3Bot failed! There is a need of a BotName!"));
                                    break;
                                }
                                string name = command[2];
                                if (!BotDatabase.Instance.HasTS3Bot(name))
                                {
                                    ConsoleOutput.Add(string.Format(">> start TS3Bot failed! BotName not found!"));
                                    break;
                                }
                                TS3BotData ts3BotData = BotDatabase.Instance.GetTs3BotData(name);
                                var list = MainServerBot.Instance.GetTs3Bots().Where(b => b.GetServer().Count() == 0);
                                if(list.Count() == 0)
                                {
                                    ConsoleOutput.Add(string.Format(">> start TS3Bot failed! No unused TS3 available!"));
                                    break;
                                }
                                TS3ClientHandler bot = list.First() as TS3ClientHandler;
                                bot.AddTS3BotData(ts3BotData);
                                break;
                            default:
                                ConsoleOutput.Add(string.Format(">> You can only start a Bot for [ts3]"));
                                break;
                        }
                    } else
                    {
                        ConsoleOutput.Add(string.Format(">> You can start a Bot for [ts3]"));
                    }
                    break;
#endif
                #endregion
                case "list":
                    if (commandSize > 2)
                    {
                        switch (command[1].ToLower())
                        {
                            case "ts3":
                                PrintTS3Bots();
                                break;
                            default:
                                break;
                        }
                    } else
                    {
                        PrintTS3Bots();
                    }
                    break;
                case "exit":
                    MainServerBot.Instance.OnTermination(null, null);
                    return;
                default:
                    ConsoleOutput.Add(">> Unknown Command");
                    break;
            }
            Console.ForegroundColor = ConsoleColor.White;
          
            // do your stuff here.
            ConsoleInput = String.Empty;
        }
        private void PrintTS3Bots()
        {
            ConsoleOutput.Add(string.Format(">> ##Ts3Bot##"));
            var ts3bots = MainServerBot.Instance.GetTs3Bots();
            foreach (var client in ts3bots)
            {
                var ts3client = client as TS3ClientHandler;
                ConsoleOutput.Add(string.Format(">> - ID: {0}", ts3client.GUID/*, client.BotClientStatus.ToString()*/));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
