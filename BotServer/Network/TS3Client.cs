﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ts3Bot.Network
{
    public class TS3Client
    {

        public UInt16 ClientID;
        public UInt64 CurrentChannelID { get; set; }
        public string Nickname { get; set; }
        public string UniqueIdentifier;
        public bool FlagSelf { get; private set; }
        private readonly int Socketid;
        private readonly UInt64 ServerConnectionHandlerID;

        public TS3Client(int Socketid, UInt64 ServerConnectionHandlerID, UInt16 clientID, ulong currentChannelID, string nickname, string uniqueIdentifier, bool flagSelf)
        {
            this.Socketid = Socketid;
            this.ServerConnectionHandlerID = ServerConnectionHandlerID;
            ClientID = clientID;
            CurrentChannelID = currentChannelID;
            Nickname = nickname;
            UniqueIdentifier = uniqueIdentifier;
            FlagSelf = flagSelf;
        }
        public void SetCurrentChannelID(UInt64 channelId)
        {
            this.CurrentChannelID = channelId;
        }
        public void SendMessage(string message)
        {
            TS3BotServerWrapper.SendMessage(Socketid, ServerConnectionHandlerID, MessageType.PRIVATE, message, ClientID);
        }
    }
}
