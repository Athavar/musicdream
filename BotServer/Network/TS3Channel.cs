﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ts3Bot.Network
{



    public class TS3Channel
    {

        public UInt64 ChannelID;
        public UInt64 ChannelParentID;
        public string Name;
        public string Topic;
        public string Description;
        public int NeededTalkPower;
        public int Codec;
        public int CodecQuality;
        public int CodecLatencyFactor;
        public bool CodecOsUnencrypted;
        public int MaxClient;
        public int MaxFamilyClient;
        public int Order;
        public bool FlagPermanant;
        public bool FlagSemiPermanent;
        public bool FlagDefault;
        public bool FlagPassword;
        public bool FlagSubscribed;
        private readonly int Socketid;
        private readonly UInt64 ServerConnectionHandlerID;

        public TS3Channel(int Socketid, UInt64 ServerConnectionHandlerID, UInt64 channelID, UInt64 channelParentID, string name, string topic, string description, int neededTalkPower, int codec, int codecQuality, int codecLatencyFactor, bool codecOsUnencrypted, int maxClient, int maxFamilyClient, int order, bool flagPermanant, bool flagSemiPermanent, bool flagDefault, bool flagPassword, bool flagSubscribed)
        {
            this.Socketid = Socketid;
            this.ServerConnectionHandlerID = ServerConnectionHandlerID;
            this.ChannelID = channelID;
            this.ChannelParentID = channelParentID;
            this.Name = name;
            this.Topic = topic;
            this.Description = description;
            this.NeededTalkPower = neededTalkPower;
            this.Codec = codec;
            this.CodecQuality = codecQuality;
            this.CodecLatencyFactor = codecLatencyFactor;
            this.CodecOsUnencrypted = codecOsUnencrypted;
            this.MaxClient = maxClient;
            this.MaxFamilyClient = maxFamilyClient;
            this.Order = order;
            this.FlagPermanant = flagPermanant;
            this.FlagSemiPermanent = flagSemiPermanent;
            this.FlagDefault = flagDefault;
            this.FlagPassword = flagPassword;
            this.FlagSubscribed = flagSubscribed;
        }

        public void SendMessage(string message)
        {
            TS3BotServerWrapper.SendMessage(Socketid, ServerConnectionHandlerID, MessageType.CHANNEL, message, ChannelID);
        }

    }
}
