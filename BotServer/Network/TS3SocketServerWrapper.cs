﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Ts3Bot.Network;

namespace Ts3Bot
{
    public static class TS3BotServerWrapper
    {
        private const string DLL = "Ts3BotSocket.dll";


        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate void SocketConnectCallback(int socketid);
        //private readonly SocketConnectCallback mSocketConnectInstance;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate void SocketDisconnectCallback(int socketid);
       // private readonly SocketDisconnectCallback mSocketDisconnectInstance;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate void ConnectStateChangeCallback(int socketid, UInt64 serverConnectionHandlerID, int state);
        //private readonly BotConnectStateChangeCallback mBotConnectStateChangeInstance;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate void NewServerDataCallback(int socketid, UInt64 serverConnectionHandlerID, string uniqueIdentifier, string name, string welcomeMessage, string platform, string version, int maxClients, bool codecEncriptionMode);
        //private readonly NewServerDataCallback mNewServerDataInstance;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate void NewChannelDataCallback(int socketid, UInt64 serverConnectionHandlerID, UInt64 channelID, UInt64 channelParentId, string name, string topic, string description, int neededTalkPower, int codec, int codecQuality,
            int codecLatencyFactor, bool codecOsUnencrypted, int maxClient, int maxFamilyClient, int Order, bool flagPermanant, bool flagSemiPermanent, bool flagDefault, bool flagPassword, bool flagSubscribed);
       // private readonly NewChannelDataCallback mNewChannelDataInstance;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate void NewClientDataCallback(int socketid, UInt64 serverConnectionHandlerID, UInt16 ClientID, UInt64 CurrentChannel, string nickname, string uniqueIdentifier, bool flagSelf);
        //private readonly NewClientDataCallback mNewClientDataInstance;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate void ClientActionCallback(int socketid, UInt64 serverConnectionHandlerID, UInt16 ClientID, UInt64 oldChannel, UInt64 newChannel, int clientAction);
        //private readonly ClientActionCallback mClientActionInstance;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate void RevMessageCallback(int socketid, UInt64 serverConnectionHandlerID, UInt16 targetMode, UInt64 targetID, string uniqueIdentifier, string message);
        //private readonly RevMessageCallback mRevMessageInstance;



        [DllImport(DLL, EntryPoint = "InitDLL")]
        private static extern void InitDLL();

        [DllImport(DLL, EntryPoint = "StartSocketServer", CallingConvention = CallingConvention.Cdecl)]
        private static extern void StartSocketServer(int port);

        [DllImport(DLL, EntryPoint = "Stop")]
        private static extern void StopSocketServer();


#if SELFCONNECT
        [DllImport(DLL, EntryPoint = "StartConnection", CallingConvention = CallingConvention.Cdecl)]
        public static extern void StartConnection(int socketid, string Identity, string ipaddrress, uint port, string nickname, string serverPassword);

        [DllImport(DLL", EntryPoint = "StopConnection", CallingConvention = CallingConvention.Cdecl)]
        public static extern void StopConnection(int socketid, UInt64 connectetServerBotHandlerID, string exitMsg);
#endif
        [DllImport(DLL, EntryPoint = "TS3SendMessage", CallingConvention = CallingConvention.Cdecl)]
        private static extern void TS3SendMessage(int socketid, UInt64 serverConnectionHandlerID, int messageType, string message, UInt64 targetID);
        
        [DllImport(DLL, EntryPoint = "ActivateAudio", CallingConvention = CallingConvention.Cdecl)]
        public static extern void ActivateAudio(int socketid, UInt64 serverConnectionHandlerID);
        
        [DllImport(DLL, EntryPoint = "SendAudioData", CallingConvention = CallingConvention.Cdecl)]
        public static extern void SendAudioData(int socketid, [MarshalAs(UnmanagedType.LPArray)]short[] buffer, int size);





        [DllImport(DLL, EntryPoint = "SetConnectCallback", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        private static extern void SetConnectCallback([MarshalAs(UnmanagedType.FunctionPtr)] SocketConnectCallback fn);

        [DllImport(DLL, EntryPoint = "SetDisconnectCallback", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        private static extern void SetDisconnectCallback([MarshalAs(UnmanagedType.FunctionPtr)] SocketDisconnectCallback fn);

        [DllImport(DLL, EntryPoint = "SetBotConnectStateChangeCallback", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        private static extern void SetBotConnectStateChangeCallback([MarshalAs(UnmanagedType.FunctionPtr)] ConnectStateChangeCallback fn);

        [DllImport(DLL, EntryPoint = "SetServerDataCallback", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        private static extern void SetNewServerDataCallback([MarshalAs(UnmanagedType.FunctionPtr)] NewServerDataCallback fn);

        [DllImport(DLL, EntryPoint = "SetChannelDataCallback", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        private static extern void SetNewChannelDataCallback([MarshalAs(UnmanagedType.FunctionPtr)] NewChannelDataCallback fn);

        [DllImport(DLL, EntryPoint = "SetClientDataCallback", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        private static extern void SetNewClientDataCallback([MarshalAs(UnmanagedType.FunctionPtr)] NewClientDataCallback fn);

        [DllImport(DLL, EntryPoint = "SetClientActionCallback", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        private static extern void SetClientActionCallback([MarshalAs(UnmanagedType.FunctionPtr)] ClientActionCallback fn);

        [DllImport(DLL, EntryPoint = "SetRevMessageCallback", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.Cdecl)]
        private static extern void SetRevMessageCallback([MarshalAs(UnmanagedType.FunctionPtr)] RevMessageCallback fn);



        #region Instance

        /*public static TS3BotServerWrapper Instance { get { return Nested.instance; } }

        private class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }

            internal static readonly TS3BotServerWrapper instance = new TS3BotServerWrapper();
        }*/

        #endregion



        private static int _port;
        private static Dictionary<int, Guid> _socketclienttable;
        
        public static void Init(int port)
        {
            _socketclienttable = new Dictionary<int, Guid>();

            //mSocketConnectInstance = new SocketConnectCallback(OnSocketConnect);
            SetConnectCallback(OnSocketConnect);

            //mSocketDisconnectInstance = new SocketDisconnectCallback(OnSocketDisconnect);
            SetDisconnectCallback(OnSocketDisconnect);

            //mBotConnectStateChangeInstance = new BotConnectStateChangeCallback(OnConnectStateChange);
            SetBotConnectStateChangeCallback(OnConnectStateChange);

            //mNewServerDataInstance = new NewServerDataCallback(OnNewServerData);
            SetNewServerDataCallback(OnNewServerData);

            //mNewChannelDataInstance = new NewChannelDataCallback(OnNewChannelData);
            SetNewChannelDataCallback(OnNewChannelData);

            //mNewClientDataInstance = new NewClientDataCallback(OnNewClientData);
            SetNewClientDataCallback(OnNewClientData);

            //mClientActionInstance = new ClientActionCallback(OnClientAction);
            SetClientActionCallback(OnClientAction);

            //mRevMessageInstance = new RevMessageCallback(OnMessage);
            SetRevMessageCallback(OnMessage);

            InitDLL();
            _port = port;
        }


        public static void Listen()
        {
            StartSocketServer(_port);
            Console.WriteLine(" >> Ts3BotServer Started on Port {0}", _port);
        }

        public static void SendMessage(int socketid, UInt64 serverConnectionHandlerID, MessageType messageType, string message, UInt64 targetID)
        {
            TS3SendMessage(socketid, serverConnectionHandlerID, (int)messageType, message, targetID);
        }



        private static readonly SocketConnectCallback OnSocketConnect =
            (int socketid) =>
        {
            TS3ClientHandler client = new TS3ClientHandler(socketid, Guid.NewGuid());
            _socketclienttable.Add(socketid, client.GUID);
            MainServerBot.Instance.AddClientHandler(client);
            _OnConnect?.Invoke(client);
        };

        private static readonly SocketDisconnectCallback OnSocketDisconnect =
            (int socketid) =>
        {
            if (_socketclienttable.ContainsKey(socketid))
            {
                _OnDisconnect?.Invoke(_socketclienttable[socketid]);
                MainServerBot.Instance.RemoveClientHandler(_socketclienttable[socketid]);
                _socketclienttable.Remove(socketid);
            }
        };

        private static readonly ConnectStateChangeCallback OnConnectStateChange =
            (int socketid, UInt64 serverConnectionHandlerID, int state) =>
        {

            if (((ConnectStatus)state) != ConnectStatus.STATUS_DISCONNECTED)
            {
                TS3Server server = MainServerBot.Instance.GetTs3Bots(socketid).GetServer(serverConnectionHandlerID);
                if (server != null) server.ServerConnectStatus = (ConnectStatus)state;
            }
            else
            {
                MainServerBot.Instance.GetTs3Bots(socketid).RemoveServer(serverConnectionHandlerID);
            }
        };

        private static readonly NewServerDataCallback OnNewServerData =
            (int socketid, UInt64 serverConnectionHandlerID, string uniqueIdentifier, string name, string welcomeMessage, string platform, string version, int maxClients, bool codecEncriptionMode) =>
        {
            TS3ClientHandler client = MainServerBot.Instance.GetTs3Bots(socketid);
            client.SetServerData(serverConnectionHandlerID, uniqueIdentifier, name, welcomeMessage, platform, version, maxClients, codecEncriptionMode);
        };

        private static readonly NewChannelDataCallback OnNewChannelData =
            (int socketid, UInt64 serverConnectionHandlerID, UInt64 channelID, UInt64 channelParentId, string name, string topic, string description, int neededTalkPower, int codec, int codecQuality,
            int codecLatencyFactor, bool codecOsUnencrypted, int maxClient, int maxFamilyClient, int Order, bool flagPermanant, bool flagSemiPermanent, bool flagDefault, bool flagPassword, bool flagSubscribed) =>
        {
            TS3ClientHandler client = MainServerBot.Instance.GetTs3Bots(socketid);
            client.SetChannelData(serverConnectionHandlerID, channelID, channelParentId, name, topic, description, neededTalkPower, codec, codecQuality,
            codecLatencyFactor, codecOsUnencrypted, maxClient, maxFamilyClient, Order, flagPermanant, flagSemiPermanent, flagDefault, flagPassword, flagSubscribed);
        };

        private static readonly NewClientDataCallback OnNewClientData =
            (int socketid, UInt64 serverConnectionHandlerID, UInt16 clientID, UInt64 currentChannelID, string nickname, string uniqueIdentifier, bool flagSelf) =>
        {
            TS3ClientHandler client = MainServerBot.Instance.GetTs3Bots(socketid);
            client.SetClientData(serverConnectionHandlerID, clientID, currentChannelID, nickname, uniqueIdentifier, flagSelf);
        };

        private static readonly ClientActionCallback OnClientAction =
            (int socketid, UInt64 serverConnectionHandlerID, UInt16 clientID, UInt64 oldChannel, UInt64 newChannel, int clientAction) =>
        {
            TS3ClientHandler handler = MainServerBot.Instance.GetTs3Bots(socketid);
            if (handler.GetServer(serverConnectionHandlerID) == null) return;
            switch ((ClientAction)clientAction)
            {
                case ClientAction.MOVE:
                    var server = handler.GetServer(serverConnectionHandlerID);
                    var client = server.GetClient(clientID);
                    client.SetCurrentChannelID(newChannel);
                    if (!server.GetChannel(oldChannel).FlagSubscribed && !server.GetChannel(oldChannel).FlagDefault)
                    {
                        var clients = server.GetClient().Where(c => c.CurrentChannelID == oldChannel).Select(c => c.ClientID).ToList();
                        foreach (var c in clients)
                            server.RemoveClient(c);
                    }
                    break;
                case ClientAction.LEAVEVISIBILITY:
                    handler.GetServer(serverConnectionHandlerID).RemoveClient(clientID);
                    break;
            }
        };

        private static readonly RevMessageCallback OnMessage =
            (int socketid, UInt64 serverConnectionHandlerID, UInt16 targetMode, UInt64 targetID, string uniqueIdentifier, string message) =>
        {
            _OnMessage?.Invoke(MainServerBot.Instance.GetTs3Bots(socketid), serverConnectionHandlerID, ClientType.TS3, (MessageType)targetMode, uniqueIdentifier, message);
        };



        public static void Exit()
        {
            StopSocketServer();
        }

        #region Event

        #region field
        private static event OnConnectDelegate _OnConnect;
        private static event OnDisconnectDelegate _OnDisconnect;
        private static event OnMessageDelegate _OnMessage;
        #endregion

        #region property
        public static event OnConnectDelegate Connect
        {
            add { _OnConnect += value; }
            remove { _OnConnect -= value; }
        }

        public static event OnDisconnectDelegate Disconnect
        {
            add { _OnDisconnect += value; }
            remove { _OnDisconnect -= value; }
        }
        public static event OnMessageDelegate Message
        {
            add { _OnMessage += value; }
            remove { _OnMessage -= value; }
        }

        #endregion

        #endregion


    }
}
