﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Ts3Bot.Audio;
using Ts3Bot.Configuration;
using Ts3Bot.Network;


static class SocketExtensions
{
    public static bool IsConnected(this Socket socket)
    {
        try
        {
            return !(socket.Poll(1, SelectMode.SelectRead) && socket.Available == 0);
        }
        catch (SocketException) { return false; }
    }
}


namespace Ts3Bot.Network
{




    //Class to handle each client request separatly
    public class TS3ClientHandler : IClientHandler
    {
        private Dictionary<UInt64, TS3Server> server_;




        public int SocketId
        {
            get; private set;
        }

        public Guid GUID
        {
            get; private set;
        }

        public AudioPlayer AudioPlayer { get; }

        public TS3ClientHandler(int socketid, Guid guid)
        {
            this.SocketId = socketid;
            this.GUID = guid;
            this.server_ = new Dictionary<ulong, TS3Server>();
            this.AudioPlayer = new AudioPlayer(2, 44100, FFmpeg.AutoGen.AVSampleFormat.AV_SAMPLE_FMT_S16);
            AudioPlayer.NewFrameData += OnNewAudioData;



            //AudioPlayer.Play(new AudioTrack("D:\\out.mp3"));
            //AudioPlayer.Play(new AudioTrack("D:\\Orion - 03 - Two Steps From Hell - Rain Of Light.mp3"));
            AudioPlayer.Repeatstate = RepeatState.SingleRepeat;
            AudioPlayer.AddToQueue(new AudioTrack("D:\\Home_Base_Groove_(ISRC_USUAN1100563).mp3"));
            //AudioPlayer.Play(new AudioTrack("D:\\out.wav"));

        }

        public IEnumerable<TS3Server> GetServer()
        {
            return server_.Select(ch => ch.Value).Cast<TS3Server>();
        }

        public TS3Server GetServer(UInt64 serverConnectionHandlerID)
        {
            return server_.ContainsKey(serverConnectionHandlerID) ? server_[serverConnectionHandlerID] : null;
        }

        public void SetServerData(UInt64 serverConnectionHandlerID, string uniqueIdentifier, string name, string welcomeMessage, string platform, string version, int maxClients, bool codecEncriptionMode)
        {

            if (!server_.ContainsKey(serverConnectionHandlerID))
            {
                server_.Add(serverConnectionHandlerID, new TS3Server(this, serverConnectionHandlerID, uniqueIdentifier, name, welcomeMessage, platform, version, maxClients, codecEncriptionMode));
                TS3BotServerWrapper.ActivateAudio(SocketId, serverConnectionHandlerID);
            }
            else
            {
                TS3Server server = server_[serverConnectionHandlerID];
                if (!server.UniqueIdentifier.Equals(uniqueIdentifier)) { server.UniqueIdentifier = uniqueIdentifier; }
                if (!server.Name.Equals(name)) { server.Name = name; }
                if (!server.WelcomeMessage.Equals(welcomeMessage)) { server.WelcomeMessage = welcomeMessage; }
                if (!server.Platform.Equals(platform)) { server.Platform = platform; }
                if (!server.Version.Equals(version)) { server.Version = version; }
                if (!server.MaxClients.Equals(maxClients)) { server.MaxClients = maxClients; }
                if (!server.CodecEncriptionMode.Equals(codecEncriptionMode)) { server.CodecEncriptionMode = codecEncriptionMode; }
            }
        }
        public void RemoveServer(UInt64 serverConnectionHandlerID)
        {
            if (server_.ContainsKey(serverConnectionHandlerID)) server_.Remove(serverConnectionHandlerID);
        }

        public void SetChannelData(UInt64 serverConnectionHandlerID, UInt64 channelID, UInt64 channelParentId, string name, string topic, string description, int neededTalkPower, int codec, int codecQuality, int codecLatencyFactor, bool codecOsUnencrypted, int maxClient, int maxFamilyClient, int order, bool flagPermanant, bool flagSemiPermanent, bool flagDefault, bool flagPassword, bool flagSubscribed)
        {
            if (!server_.ContainsKey(serverConnectionHandlerID)) return;
            
            if (server_[serverConnectionHandlerID].HasChannel(channelID))
            {
                if (channelParentId == UInt64.MaxValue - 1)
                {
                    server_[serverConnectionHandlerID].RemoveChannel(channelID);
                } else
                {
                    TS3Channel channel = server_[serverConnectionHandlerID].GetChannel(channelID);
                    if (channelParentId != UInt64.MaxValue && !channel.ChannelParentID.Equals(channelParentId)) { channel.ChannelParentID = channelParentId; }
                    if (!channel.Name.Equals(name)) { channel.Name = name; }
                    if (!channel.Topic.Equals(topic)) { channel.Topic = topic; }
                    if (!channel.Description.Equals(description)) { channel.Description = description; }
                    if (channel.NeededTalkPower != neededTalkPower) { channel.NeededTalkPower = neededTalkPower; }
                    if (channel.Codec != codec) { channel.Codec = codec; }
                    if (channel.CodecLatencyFactor != codecLatencyFactor) { channel.CodecLatencyFactor = codecLatencyFactor; }
                    if (channel.CodecOsUnencrypted != codecOsUnencrypted) { channel.CodecOsUnencrypted = codecOsUnencrypted; }
                    if (channel.MaxClient != maxClient) { channel.MaxClient = maxClient; }
                    if (channel.MaxFamilyClient != maxFamilyClient) { channel.MaxFamilyClient = maxFamilyClient; }
                    if (channel.Order != order) { channel.Order = order; }
                    if (channel.FlagPermanant != flagPermanant) { channel.FlagPermanant = flagPermanant; }
                    if (channel.FlagSemiPermanent != flagSemiPermanent) { channel.FlagSemiPermanent = flagSemiPermanent; }
                    if (channel.FlagDefault != flagDefault) { channel.FlagDefault = flagDefault; }
                    if (channel.FlagPassword != flagPassword) { channel.FlagPassword = flagPassword; }
                    if (channel.FlagSubscribed != flagSubscribed) { channel.FlagSubscribed = flagSubscribed; }
                }
            }
            else
            {
                server_[serverConnectionHandlerID].AddChannel(new TS3Channel(SocketId, serverConnectionHandlerID, channelID, channelParentId, name, topic, description, neededTalkPower, codec, codecQuality, codecLatencyFactor, codecOsUnencrypted, maxClient, maxFamilyClient, order, flagPermanant, flagSemiPermanent, flagDefault, flagPassword, flagSubscribed));
            }
        }

        internal void SetClientData(UInt64 serverConnectionHandlerID, UInt16 clientID, UInt64 currentChannelID, string nickname, string uniqueIdentifier, bool flagSelf)
        {
            if (!server_.ContainsKey(serverConnectionHandlerID)) return;
            if (server_[serverConnectionHandlerID].HasClient(clientID))
            {
                TS3Client client = server_[serverConnectionHandlerID].GetClient(clientID);
                if (!client.CurrentChannelID.Equals(currentChannelID)) { client.CurrentChannelID = currentChannelID; }
                if (!client.Nickname.Equals(nickname)) { client.Nickname = nickname; }
                if (!client.UniqueIdentifier.Equals(uniqueIdentifier)) { client.UniqueIdentifier = uniqueIdentifier; }
            }
            else
            {
                server_[serverConnectionHandlerID].AddClient(new TS3Client(SocketId, serverConnectionHandlerID, clientID, currentChannelID, nickname, uniqueIdentifier, flagSelf));
            }
        }

        private void OnNewAudioData(short[] data) {
            TS3BotServerWrapper.SendAudioData(SocketId, data, data.Length * sizeof(short));

        }


#if SELFCONNECT
        public bool AddTS3BotData(TS3BotData ts3BotData)
        {
            TS3Server server = new TS3Server(this, ts3BotData);
            server_.Add(server.ServerConnectionHandlerID, server);
            return true;
        }


        /*
        public void ConnectBot()
        {
            TS3BotServerWrapper.StartConnection(this.SocketId, this.Ts3BotData.Identity, this.Ts3BotData.IpAddress, this.Ts3BotData.Port, this.Ts3BotData.Nickname, this.Ts3BotData.serverPassword);
        }

        public void DisconnectBot()
        {
            TS3BotServerWrapper.StopConnection(SocketId, ServerHandlerID, Ts3BotData.ExitMsg);
        }*/
#endif
    }
}
