﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ts3Bot.Audio;

namespace Ts3Bot.Network
{
    public delegate void OnConnectDelegate(IClientHandler guid);
    public delegate void OnDisconnectDelegate(Guid guid);
    public delegate void OnMessageDelegate(IClientHandler handler, UInt64 connectID,  ClientType clientType, MessageType messageType, string senderID, string message);

    public enum ConnectStatus : int
    {
        STATUS_DISCONNECTED = 0x00, STATUS_CONNECTING = 0x01, STATUS_CONNECTED = 0x02, STATUS_CONNECTION_ESTABLISHING = 0x03, STATUS_CONNECTION_ESTABLISHED = 0x4
    }

    public enum SocketState
    {
        CONNECT = 0, CLOSED = 1
    };
    public enum ClientAction
    {
        MOVE = 0, LEAVEVISIBILITY = 1
    };

    public enum MessageType {
        PRIVATE = 1, CHANNEL = 2
    };

    public enum ClientType
    {
        TS3, DISCORD
    };


    public interface IClientHandler
    {
        Guid GUID
        {
            get;
        }
        AudioPlayer AudioPlayer
        {
            get;
        }

    }
}
