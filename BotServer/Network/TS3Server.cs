﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ts3Bot.Configuration;

namespace Ts3Bot.Network
{
    public class TS3Server
    {
        private readonly TS3ClientHandler client_;
        private Dictionary<UInt64, TS3Channel> ChannelList_ = new Dictionary<ulong, TS3Channel>();
        private Dictionary<UInt64, TS3Client> ClientList_ = new Dictionary<ulong, TS3Client>();
        public UInt64 ServerConnectionHandlerID { get; private set; }
        public string UniqueIdentifier { get; set; }
        public string Name { get; set; }
        public string WelcomeMessage { get; set; }
        public string Platform { get; set; }
        public string Version { get; set; }
        public int MaxClients { get; set; }
        public bool CodecEncriptionMode{ get; set; }
        public ConnectStatus ServerConnectStatus { get; set; }
        
        public TS3Server(TS3ClientHandler client, UInt64 serverConnectionHandlerID, string uniqueIdentifier, string name, string welcomeMessage, string platform, string version, int maxClients, bool codecEncriptionMode)
        {
            this.client_ = client;
            this.ServerConnectionHandlerID = serverConnectionHandlerID;
            this.UniqueIdentifier = uniqueIdentifier;
            this.Name = name;
            this.WelcomeMessage = welcomeMessage;
            this.Platform = platform;
            this.Version = version;
            this.MaxClients = maxClients;
            this.CodecEncriptionMode = codecEncriptionMode;
        }
        public void SetServerData(UInt64 serverConnectionHandlerID, string uniqueIdentifier, string name, string welcomeMessage, string platform, string version, int maxClients, bool codecEncriptionMode)
        {
            this.ServerConnectionHandlerID = serverConnectionHandlerID;
            this.UniqueIdentifier = uniqueIdentifier;
            this.Name = name;
            this.WelcomeMessage = welcomeMessage;
            this.Platform = platform;
            this.Version = version;
            this.MaxClients = maxClients;
            this.CodecEncriptionMode = codecEncriptionMode;
        }



        public IEnumerable<TS3Channel> GetChannel()
        {
            return ChannelList_.Select(ch => ch.Value).Cast<TS3Channel>();
        }
        public bool HasChannel(UInt64 channelId)
        {
            return ChannelList_.ContainsKey(channelId);
        }
        public TS3Channel GetChannel(UInt64 channelId)
        {
            return ChannelList_[channelId];
        }
        public void AddChannel(TS3Channel channel)
        {   if(!ChannelList_.ContainsKey(channel.ChannelID))
                ChannelList_.Add(channel.ChannelID, channel);
        }
        public void RemoveChannel(UInt64 channelID)
        {
            if (ChannelList_.ContainsKey(channelID))
                ChannelList_.Remove(channelID);
        }


        public IEnumerable<TS3Client> GetClient()
        {
            return ClientList_.Select(ch => ch.Value).Cast<TS3Client>();
        }
        public bool HasClient(UInt64 clientId)
        {
            return ClientList_.ContainsKey(clientId);
        }
        public bool HasClient(string uniqueIdentifier)
        {
            return ClientList_.Where(c => c.Value.UniqueIdentifier == uniqueIdentifier).Any();
        }

        public TS3Client GetClient(UInt64 clientId)
        {
            return ClientList_[clientId];
        }

        public TS3Client GetClient(string uniqueIdentifier)
        {
            return ClientList_.Where(c => c.Value.UniqueIdentifier == uniqueIdentifier).Select(kv => kv.Value).First();
        }
        public void AddClient(TS3Client client)
        {
            if (!ClientList_.ContainsKey(client.ClientID))
                ClientList_.Add(client.ClientID, client);
        }
        public void RemoveClient(UInt64 clientID)
        {
            if (ClientList_.ContainsKey(clientID))
                ClientList_.Remove(clientID);
        }





#if SELFCONNECT
        
        public TS3Server(TS3ClientHandler client, TS3BotData ts3BotData)
        {
            this.client_ = client;
            this.Ts3BotData = ts3BotData;
            Connect();

        }

        public TS3BotData Ts3BotData
        {
            get; private set;
        }

        public void Connect()
        {
            if(Ts3BotData.IpAddress != "")
            {
                TS3BotServerWrapper.StartConnection(client_.SocketId, Ts3BotData.Identity, Ts3BotData.IpAddress, Ts3BotData.Port, Ts3BotData.Nickname, Ts3BotData.serverPassword);
            }
        }


        public void Disconnect()
        {
            TS3BotServerWrapper.StopConnection(client_.SocketId, ServerConnectionHandlerID, "");
        }

#endif

    }
}
