/*
 * TeamSpeak 3 demo plugin
 *
 * Copyright (c) 2008-2017 TeamSpeak Systems GmbH
 */
#include "plugin.h"

struct TS3Functions ts3Functions;

#ifdef _WIN32
#define _strcpy(dest, destSize, src) strcpy_s(dest, destSize, src)
#define snprintf sprintf_s
#else
#define _strcpy(dest, destSize, src) { strncpy(dest, src, destSize-1); (dest)[destSize-1] = '\0'; }
#endif

#define PLUGIN_API_VERSION 22

#define PATH_BUFSIZE 512
#define COMMAND_BUFSIZE 128
#define INFODATA_BUFSIZE 128
#define SERVERINFO_BUFSIZE 256
#define CHANNELINFO_BUFSIZE 512
#define RETURNCODE_BUFSIZE 128

static char* pluginID = NULL;

std::map<uint64, anyID> selfClientIds;




#ifdef _WIN32
/* Helper function to convert wchar_T to Utf-8 encoded strings on Windows */
static int wcharToUtf8(const wchar_t* str, char** result) {
	int outlen = WideCharToMultiByte(CP_UTF8, 0, str, -1, 0, 0, 0, 0);
	*result = (char*)malloc(outlen);
	if(WideCharToMultiByte(CP_UTF8, 0, str, -1, *result, outlen, 0, 0) == 0) {
		*result = NULL;
		return -1;
	}
	return 0;
}
#endif

/*********************************** Required functions ************************************/
/*
 * If any of these required functions is not implemented, TS3 will refuse to load the plugin
 */

/* Unique name identifying this plugin */
const char* ts3plugin_name() {
#ifdef _WIN32
	/* TeamSpeak expects UTF-8 encoded characters. Following demonstrates a possibility how to convert UTF-16 wchar_t into UTF-8. */
	static char* result = NULL;  /* Static variable so it's allocated only once */
	if(!result) {
		const wchar_t* name = L"Ts3BotPlugin";
		if(wcharToUtf8(name, &result) == -1) {  /* Convert name into UTF-8 encoded result */
			result = "Ts3BotPlugin";  /* Conversion failed, fallback here */
		}
	}
	return result;
#else
	return "Ts3BotPlugin";
#endif
}

/* Plugin version */
const char* ts3plugin_version() {
    return "1.2";
}

/* Plugin API version. Must be the same as the clients API major version, else the plugin fails to load. */
int ts3plugin_apiVersion() {
	return PLUGIN_API_VERSION;
}

/* Plugin author */
const char* ts3plugin_author() {
	/* If you want to use wchar_t, see ts3plugin_name() on how to use */
    return "Athavar Greentree";
}

/* Plugin description */
const char* ts3plugin_description() {
	/* If you want to use wchar_t, see ts3plugin_name() on how to use */
    return "Ts3BotPlugin to Bot Ts3";
}

/* Set TeamSpeak 3 callback functions */
void ts3plugin_setFunctionPointers(const struct TS3Functions funcs) {
    ts3Functions = funcs;
}


#ifdef SELFCONNECT
TS3StartConnectionCallback * mTS3StartConnectionInstance;
TS3StopConnectionCallback * mTS3StopConnectionInstance;

void TS3StartConnection(char * Identity, char * ipaddress, unsigned int port, char * nickname, char ** defaultChannelArray, char * defaultChannelPassword, char * serverPassword)
{
	uint64 connectetServerBotHandlerID;
	int error;
	char * ret;
	char * id;
	error = ts3Functions.spawnNewServerConnectionHandler(0, &connectetServerBotHandlerID);
	if(error != ERROR_ok)
	{
		ThrowTS3ERROR(error);
	}
	error = ts3Functions.startConnection(connectetServerBotHandlerID, Identity, ipaddress, port, nickname, NULL, "", serverPassword);
	if (error != ERROR_ok)
	{
		ThrowTS3ERROR(error);
	}
	error = ts3Functions.requestServerGroupList(connectetServerBotHandlerID, ret);
	if (error != ERROR_ok)
	{
		ThrowTS3ERROR(error);
	}
	error = ts3Functions.requestChannelGroupList(connectetServerBotHandlerID, ret);
	if (error != ERROR_ok)
	{
		ThrowTS3ERROR(error);
	}
	error = ts3Functions.requestClientIDs(connectetServerBotHandlerID, id,  ret);
	if (error != ERROR_ok)
	{
		ThrowTS3ERROR(error);
	}
}

void TS3StopConnection(uint64 connectetServerBotHandlerID, char * msg)
{
	unsigned int error;
	error = ts3Functions.stopConnection(connectetServerBotHandlerID, msg );
	if (error != ERROR_ok)
	{
		ThrowTS3ERROR(error);
	}
	error = ts3Functions.destroyServerConnectionHandler(connectetServerBotHandlerID);
	if (error != ERROR_ok)
	{
		ThrowTS3ERROR(error);
	}
}
#endif


void sendServerData(uint64 serverConnectionHandlerID);
void sendChannelData(uint64 serverConnectionHandlerID, uint64 channelID, uint64 channelParentID, bool withrequests);
void sendClientData(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility);

TS3ReSendAllDataCallback * mTS3ReSendAllDataInstance;
TS3SendChannelTextMessageCallback * mTS3SendChannelTextMessageInstance;
TS3SendPrivateTextMessageCallback * mTS3SendPrivateTextMessageInstance;
TS3ActivateAudioCallback * mTS3ActivateAudioInstance;
TS3SendAudioDataCallback * mTS3SendAudioDataInstance;


void TS3ReSendAllData() {
	int results; 
	uint64 * serverConnectionHandlerIDs, * channelIDs, parentChannelID;
	int i = 0; 
	anyID myID, * clientIDs;
	if (ts3Functions.getServerConnectionHandlerList(&serverConnectionHandlerIDs) == ERROR_ok) {
		for (int iserver = 0; serverConnectionHandlerIDs[iserver]; iserver++)
		{
			if (ts3Functions.getConnectionStatus(serverConnectionHandlerIDs[iserver], &results) != ERROR_ok && results == 0) continue;

			if (ts3Functions.getClientID(serverConnectionHandlerIDs[iserver], &myID) == ERROR_ok)
			{
				selfClientIds[serverConnectionHandlerIDs[iserver]] = myID;
			} else continue;

			sendServerData(serverConnectionHandlerIDs[iserver]);

			if (ts3Functions.getChannelList(serverConnectionHandlerIDs[iserver], &channelIDs) == ERROR_ok) {
				for (int ichannel = 0; channelIDs[ichannel]; ichannel++)
				{
					ts3Functions.getParentChannelOfChannel(serverConnectionHandlerIDs[iserver], channelIDs[ichannel], &parentChannelID);
					sendChannelData(serverConnectionHandlerIDs[iserver], channelIDs[ichannel], parentChannelID, true);

					if (ts3Functions.getChannelClientList(serverConnectionHandlerIDs[iserver], channelIDs[ichannel], &clientIDs) == ERROR_ok) {
						for (int iclient = 0; clientIDs[iclient]; iclient++)
						{
							sendClientData(serverConnectionHandlerIDs[iserver], clientIDs[iclient], 0, channelIDs[ichannel], ENTER_VISIBILITY);
						}
					}
				}
			}
		}
		ts3Functions.freeMemory(serverConnectionHandlerIDs);
	}


}

void TS3SendChannelTextMessage(uint64 serverConnectionHandlerID, char * message, uint64 targetChannelID) {
	char * retCode;
	unsigned int error = ts3Functions.requestSendChannelTextMsg(serverConnectionHandlerID, message, targetChannelID, retCode);
	if (error == ERROR_ok) {
		ThrowTS3ERROR(error);
	}
}

void TS3SendPrivateTextMessage(uint64 serverConnectionHandlerID, char * message, anyID targetClientID) {
	char * retCode;
	unsigned int error = ts3Functions.requestSendPrivateTextMsg(serverConnectionHandlerID, message, targetClientID, retCode);
	if (error == ERROR_ok) {
		ThrowTS3ERROR(error);
	}
}


void TS3ActivateAudio(uint64 serverConnectionHandlerID) {
	unsigned int error;
	error = ts3Functions.closeCaptureDevice(serverConnectionHandlerID);
	if (error != ERROR_ok) {
		ThrowTS3ERROR(error);
	}
	error = ts3Functions.openCaptureDevice(serverConnectionHandlerID, "custom", CUSTROMMICRO_DEVICEID);
	if (error != ERROR_ok) {
		ThrowTS3ERROR(error);
	}
	error = ts3Functions.activateCaptureDevice(serverConnectionHandlerID);
	if (error != ERROR_ok) {
		ThrowTS3ERROR(error);
	}


}

void TS3SendSoundData(short * buffer, int samples) {
	unsigned int error = ts3Functions.processCustomCaptureData(CUSTROMMICRO_DEVICEID, buffer, samples/2);
	if (error != ERROR_ok) {
		ThrowTS3ERROR(error);
	}
}


void sendServerData(uint64 serverConnectionHandlerID)
{
	int i;
	char * s;
	PackageServerData * package = new PackageServerData();
	package->_PID = PID::SERVERDATA;
	package->serverConnectionHandlerID = serverConnectionHandlerID;
	if(ts3Functions.getServerVariableAsString(serverConnectionHandlerID, VIRTUALSERVER_NAME, &s) == ERROR_ok){
		XMemCopyChar(package->Name, s);
		ts3Functions.freeMemory(s);
	}
	if(ts3Functions.getServerVariableAsString(serverConnectionHandlerID, VIRTUALSERVER_WELCOMEMESSAGE, &s) == ERROR_ok){
		XMemCopyChar(package->WelcomeMessage, s);
		ts3Functions.freeMemory(s);
	}
	if(ts3Functions.getServerVariableAsString(serverConnectionHandlerID, VIRTUALSERVER_UNIQUE_IDENTIFIER, &s) == ERROR_ok){
		XMemCopyChar(package->UniqueIdentifier, s);
		ts3Functions.freeMemory(s);
	}
	if(ts3Functions.getServerVariableAsString(serverConnectionHandlerID, VIRTUALSERVER_PLATFORM, &s) == ERROR_ok){
		XMemCopyChar(package->Platform, s);
		ts3Functions.freeMemory(s);
	}
	if(ts3Functions.getServerVariableAsString(serverConnectionHandlerID, VIRTUALSERVER_VERSION, &s) == ERROR_ok){
		XMemCopyChar(package->Version, s);
		ts3Functions.freeMemory(s);
	}
	if(ts3Functions.getServerVariableAsInt(serverConnectionHandlerID, VIRTUALSERVER_MAXCLIENTS, &i) == ERROR_ok){
		package->MaxClients = i;
	}
	if(ts3Functions.getServerVariableAsInt(serverConnectionHandlerID, VIRTUALSERVER_CODEC_ENCRYPTION_MODE, &i) == ERROR_ok){
		package->CodecEncriptionMode = i;
	}
	SendPackage(-1, package, sizeof(PackageServerData));
	delete package;
}

void sendChannelData(uint64 serverConnectionHandlerID, uint64 channelID, uint64 channelParentID, bool withrequests = false)
{
	char * s;
	int i;
	PackageChannelData * package = new PackageChannelData();
	package->_PID = PID::CHANNELDATA;
	package->serverConnectionHandlerID = serverConnectionHandlerID;
	package->ChannelId = channelID;
	package->ChannelParentId = channelParentID;
	if (ts3Functions.getChannelVariableAsString(serverConnectionHandlerID, channelID, CHANNEL_NAME, &s) == ERROR_ok)
	{
		XMemCopyChar(package->Name, s);
		ts3Functions.freeMemory(s);
	}
	;
	if (/*ts3Functions.requestChannelDescription(serverConnectionHandlerID, channelID, &recode) == ERROR_ok  &&*/ ts3Functions.getChannelVariableAsString(serverConnectionHandlerID, channelID, CHANNEL_DESCRIPTION, &s) == ERROR_ok)
	{
		XMemCopyChar(package->Description, s);
		ts3Functions.freeMemory(s);
	}
	if (ts3Functions.getChannelVariableAsString(serverConnectionHandlerID, channelID, CHANNEL_TOPIC, &s) == ERROR_ok)
	{
		XMemCopyChar(package->Topic, s);
		ts3Functions.freeMemory(s);
	}
	if (ts3Functions.getChannelVariableAsInt(serverConnectionHandlerID, channelID, CHANNEL_NEEDED_TALK_POWER, &i) == ERROR_ok)
	{
		package->NeededTalkPower = i;
	}
	if (ts3Functions.getChannelVariableAsInt(serverConnectionHandlerID, channelID, CHANNEL_CODEC, &i) == ERROR_ok)
	{
		package->Codec = i;
	}
	if (ts3Functions.getChannelVariableAsInt(serverConnectionHandlerID, channelID, CHANNEL_CODEC_QUALITY, &i) == ERROR_ok)
	{
		package->CodecQuality = i;
	}
	if (ts3Functions.getChannelVariableAsInt(serverConnectionHandlerID, channelID, CHANNEL_CODEC_LATENCY_FACTOR, &i) == ERROR_ok)
	{
		package->CodecLatencyFactor = i;
	}
	if (ts3Functions.getChannelVariableAsInt(serverConnectionHandlerID, channelID, CHANNEL_CODEC_IS_UNENCRYPTED, &i) == ERROR_ok)
	{
		package->CodecIsUnencrypted = i;
	}
	if (ts3Functions.getChannelVariableAsInt(serverConnectionHandlerID, channelID, CHANNEL_MAXCLIENTS, &i) == ERROR_ok)
	{
		package->MaxClients = i;
	}
	if (ts3Functions.getChannelVariableAsInt(serverConnectionHandlerID, channelID, CHANNEL_MAXFAMILYCLIENTS, &i) == ERROR_ok)
	{
		package->MaxFamilyClients = i;
	}
	if (ts3Functions.getChannelVariableAsInt(serverConnectionHandlerID, channelID, CHANNEL_ORDER, &i) == ERROR_ok)
	{
		package->Order = i;
	}
	if (ts3Functions.getChannelVariableAsInt(serverConnectionHandlerID, channelID, CHANNEL_FLAG_PERMANENT, &i) == ERROR_ok)
	{
		package->FlagPermanent = i;
	}
	if (ts3Functions.getChannelVariableAsInt(serverConnectionHandlerID, channelID, CHANNEL_FLAG_SEMI_PERMANENT, &i) == ERROR_ok)
	{
		package->FlagSemiPermanent = i;
	}
	if (ts3Functions.getChannelVariableAsInt(serverConnectionHandlerID, channelID, CHANNEL_FLAG_DEFAULT, &i) == ERROR_ok)
	{
		package->FlagDefault = i;
	}
	if (ts3Functions.getChannelVariableAsInt(serverConnectionHandlerID, channelID, CHANNEL_FLAG_PASSWORD, &i) == ERROR_ok)
	{
		package->FlagPassword = i;
	}
	if (ts3Functions.getChannelVariableAsInt(serverConnectionHandlerID, channelID, CHANNEL_FLAG_ARE_SUBSCRIBED, &i) == ERROR_ok)
	{
		package->FlagSubscribed = i;
	}
	SendPackage(-1, package, sizeof(PackageChannelData));
	delete package;
}

void sendClientData(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility)
{
	if (selfClientIds[serverConnectionHandlerID] == clientID && oldChannelID != 0 && newChannelID != 0) {
		int isSubscribed;

		if (ts3Functions.getChannelVariableAsInt(serverConnectionHandlerID, oldChannelID, CHANNEL_FLAG_ARE_SUBSCRIBED, &isSubscribed) != ERROR_ok) {
			/* Handle error */
		}
		else if (!isSubscribed) {
			//

		}
	}

	if (visibility == ENTER_VISIBILITY) {

		char * s;
		int i;
		PackageClientData * package = new PackageClientData();
		package->_PID = PID::CLIENTDATA;
		package->serverConnectionHandlerID = serverConnectionHandlerID;
		package->ClientId = clientID;
		package->CurrentChannelID = newChannelID;
		package->FlagSelf = selfClientIds[serverConnectionHandlerID] == clientID;

		if (ts3Functions.getClientVariableAsString(serverConnectionHandlerID, clientID, CLIENT_UNIQUE_IDENTIFIER, &s) == ERROR_ok) {
			XMemCopyChar(package->UniqueIdentifier, s);
			ts3Functions.freeMemory(s);
		}
		if (ts3Functions.getClientVariableAsString(serverConnectionHandlerID, clientID, CLIENT_NICKNAME, &s) == ERROR_ok) {
			XMemCopyChar(package->Nickname, s);
			ts3Functions.freeMemory(s);
		}
		SendPackage(-1, package, sizeof(PackageClientData));
		delete package;

	}
	else {
		PackageClientAction * package = new PackageClientAction();
		package->_PID = PID::CLIENTACTION;
		package->serverConnectionHandlerID = serverConnectionHandlerID;
		package->ClientId = clientID;
		package->oldChannelID = oldChannelID;
		package->newChannelID = newChannelID;
		if (visibility == RETAIN_VISIBILITY) {
			//action
			package->Action = ClientAction::MOVE;
		}
		if (visibility == LEAVE_VISIBILITY) {
			package->Action = ClientAction::LEAVEVISIBILITY;
		}
		SendPackage(-1, package, sizeof(PackageClientAction));
		delete package;
	}


}


/*
 * Custom code called right after loading the plugin. Returns 0 on success, 1 on failure.
 * If the function returns 1 on failure, the plugin will be unloaded again.
 */
int ts3plugin_init() {
    char appPath[PATH_BUFSIZE];
    char resourcesPath[PATH_BUFSIZE];
    char configPath[PATH_BUFSIZE];
	char pluginPath[PATH_BUFSIZE];

    /* Your plugin init code here */
    printf("PLUGIN: init\n");

    /* Example on how to query application, resources and configuration paths from client */
    /* Note: Console client returns empty string for app and resources path */
    ts3Functions.getAppPath(appPath, PATH_BUFSIZE);
    ts3Functions.getResourcesPath(resourcesPath, PATH_BUFSIZE);
    ts3Functions.getConfigPath(configPath, PATH_BUFSIZE);
	ts3Functions.getPluginPath(pluginPath, PATH_BUFSIZE, pluginID);

	printf("PLUGIN: App path: %s\nResources path: %s\nConfig path: %s\nPlugin path: %s\n", appPath, resourcesPath, configPath, pluginPath);
	
	//outputFile = std::ofstream("D:\\StackSize");


	InitDLL(false);


#ifdef SELFCONNECT
	mTS3StartConnectionInstance = new TS3StartConnectionCallback((TS3StartConnectionCallback)TS3StartConnection);
	SetTS3StartConnectionCallback(*mTS3StartConnectionInstance);

	mTS3StopConnectionInstance = new TS3StopConnectionCallback((TS3StopConnectionCallback)TS3StopConnection);
	SetTS3StopConnectionCallback(*mTS3StopConnectionInstance);
#endif


	mTS3SendChannelTextMessageInstance = new TS3SendChannelTextMessageCallback((TS3SendChannelTextMessageCallback)&TS3SendChannelTextMessage);
	SetTS3SendChannelTextMessageCallback(*mTS3SendChannelTextMessageInstance);

	mTS3SendPrivateTextMessageInstance = new TS3SendPrivateTextMessageCallback((TS3SendPrivateTextMessageCallback)&TS3SendPrivateTextMessage);
	SetTS3SendPrivateTextMessageCallback(*mTS3SendPrivateTextMessageInstance);


	mTS3ReSendAllDataInstance = new TS3ReSendAllDataCallback((TS3ReSendAllDataCallback)&TS3ReSendAllData);
	SetTS3ReSendAllDataCallback(*mTS3ReSendAllDataInstance);

	mTS3ActivateAudioInstance = new TS3ActivateAudioCallback((TS3ActivateAudioCallback)&TS3ActivateAudio);
	SetTS3ActivateAudioCallback(*mTS3ActivateAudioInstance);

	mTS3SendAudioDataInstance = new TS3SendAudioDataCallback((TS3SendAudioDataCallback)&TS3SendSoundData);
	SetTS3SendAudioDataCallback(*mTS3SendAudioDataInstance);

	unsigned int error = ts3Functions.registerCustomDevice(CUSTROMMICRO_DEVICEID, CUSTROMMICRO_DEVICENAME, 44100, 2, 44100, 1);
	if (error != ERROR_ok) {
		ThrowTS3ERROR(error);
	}
	
	printf("\nread Config for SOCKET...");
	std::string line, ipaddr;
	std::ifstream infile(SOCKETSETTINGFILEPATH);
	if (infile.good())
	{
		char * temp[16];
		if (!(!std::getline(infile, line) || inet_pton(AF_INET, line.c_str(), temp)<=0))
		{
			ipaddr = line.c_str();
			if (!(!std::getline(infile, line) || line.empty() || line.length() > 5 || std::find_if(line.begin(), line.end(), [](char c) { return !std::isdigit(c); }) != line.end() || std::stoul (line,nullptr,10) > 65535))
			{
				int val = std::stoul(line, nullptr, 10);
				StartSocketClient(ipaddr, val);

			}
			else
			{
				printf("Invalid Port/ Port not supported in the second line of config");
			}
		} else {
			printf("Invalid address/ Address not supported in the first line of config");
		}
	} else {
		printf("\ncan't read file \"%s\"",SOCKETSETTINGFILEPATH);
	}


	



	
    return 0;  /* 0 = success, 1 = failure, -2 = failure but client will not show a "failed to load" warning */
	/* -2 is a very special case and should only be used if a plugin displays a dialog (e.g. overlay) asking the user to disable
	 * the plugin again, avoiding the show another dialog by the client telling the user the plugin failed to load.
	 * For normal case, if a plugin really failed to load because of an error, the correct return value is 1. */
}

/* Custom code called right before the plugin is unloaded */
void ts3plugin_shutdown() {
    /* Your plugin cleanup code here */
    printf("PLUGIN: shutdown\n");
	unsigned int error = ts3Functions.unregisterCustomDevice(CUSTROMMICRO_DEVICEID);
	if (error != ERROR_ok) {
		ThrowTS3ERROR(error);
	}
	Stop();


#ifdef SELFCONNECT
	uint64* ids;
	unsigned int error = ts3Functions.getServerConnectionHandlerList(&ids);
	if(error == ERROR_ok)
	{
		for(int i=0; ids[i]; i++) 
		{
			ts3Functions.stopConnection(ids[i],"EXIT CLIENT");
			ts3Functions.destroyServerConnectionHandler(ids[i]);
		}
		ts3Functions.freeMemory(ids);
	} else ThrowTS3ERROR(error);
#endif 

	/*
	 * Note:
	 * If your plugin implements a settings dialog, it must be closed and deleted here, else the
	 * TeamSpeak client will most likely crash (DLL removed but dialog from DLL code still open).
	 */

	/* Free pluginID if we registered it */
	if(pluginID) {
		free(pluginID);
		pluginID = NULL;
	}
}

/****************************** Optional functions ********************************/
/*
 * Following functions are optional, if not needed you don't need to implement them.
 */

int ts3plugin_offersConfigure() {
	return PLUGIN_OFFERS_NO_CONFIGURE;
}

void ts3plugin_registerPluginID(const char* id) {
	const size_t sz = strlen(id) + 1;
	pluginID = (char*)malloc(sz * sizeof(char));
	_strcpy(pluginID, sz, id);  /* The channelID buffer will invalidate after exiting this function */
	printf("PLUGIN: registerPluginID: %s\n", pluginID);
}

const char* ts3plugin_commandKeyword() {
	return "";
}

int ts3plugin_processCommand(uint64 serverConnectionHandlerID, const char* command) {
	return 1; 
}

void ts3plugin_currentServerConnectionChanged(uint64 serverConnectionHandlerID) {
    printf("PLUGIN: currentServerConnectionChanged %llu (%llu)\n", (long long unsigned int)serverConnectionHandlerID, (long long unsigned int)ts3Functions.getCurrentServerConnectionHandlerID());
}

const char* ts3plugin_infoTitle() {
	return "Test plugin info";
}

void ts3plugin_infoData(uint64 serverConnectionHandlerID, uint64 id, enum PluginItemType type, char** data) {
	data = NULL;
	return;
	
}

void ts3plugin_freeMemory(void* data) {
	free(data);
}

int ts3plugin_requestAutoload() {
	return 0;  /* 1 = request autoloaded, 0 = do not request autoload */
}

/************************** TeamSpeak callbacks ***************************/
/*
 * Following functions are optional, feel free to remove unused callbacks.
 * See the clientlib documentation for details on each function.
 */

/* Clientlib */

void ts3plugin_onConnectStatusChangeEvent(uint64 serverConnectionHandlerID, int newStatus, unsigned int errorNumber) {
    /* Some example code following to show how to use the information query functions. */
	PackageConnectStatus * package = new PackageConnectStatus();

	package->_PID = PID::CONNECTIONSTATE;
	package->serverConnectionHandlerID = serverConnectionHandlerID;
	package->ConnectionState = (TSConnectStatus) newStatus;
	SendPackage(-1, package, sizeof(PackageConnectStatus));
	delete package;

	if(newStatus == STATUS_CONNECTED)
	{
		sendServerData(serverConnectionHandlerID);
		anyID clientID;
		unsigned int error = ts3Functions.getClientID(serverConnectionHandlerID, &clientID);
		if (error != ERROR_ok) {
			ThrowTS3ERROR(error);
		} else 
		selfClientIds[serverConnectionHandlerID] = clientID;
	}



    if(newStatus == STATUS_CONNECTION_ESTABLISHED) {  /* connection established and we have client and channels available */
        
	}
	else if (newStatus == STATUS_DISCONNECTED)
	{
		selfClientIds.erase(serverConnectionHandlerID);
	}
}

void ts3plugin_onNewChannelEvent(uint64 serverConnectionHandlerID, uint64 channelID, uint64 channelParentID) {
	sendChannelData(serverConnectionHandlerID, channelID, channelParentID, true);
	anyID* channelClientList;
	unsigned int error = ts3Functions.getChannelClientList(serverConnectionHandlerID, channelID, &channelClientList);
	if(error != ERROR_ok)
	{
		ThrowTS3ERROR(error);
		return;
	}
	for (int i = 0; channelClientList[i]; i++)
	{
		sendClientData(serverConnectionHandlerID, channelClientList[i], 0, channelID, ENTER_VISIBILITY);
	}
}

void ts3plugin_onNewChannelCreatedEvent(uint64 serverConnectionHandlerID, uint64 channelID, uint64 channelParentID, anyID invokerID, const char* invokerName, const char* invokerUniqueIdentifier) {
	sendChannelData(serverConnectionHandlerID, channelID, channelParentID);
}

void ts3plugin_onDelChannelEvent(uint64 serverConnectionHandlerID, uint64 channelID, anyID invokerID, const char* invokerName, const char* invokerUniqueIdentifier) {
	sendChannelData(serverConnectionHandlerID, channelID, MAXUINT64 - 1);
}

void ts3plugin_onChannelMoveEvent(uint64 serverConnectionHandlerID, uint64 channelID, uint64 newChannelParentID, anyID invokerID, const char* invokerName, const char* invokerUniqueIdentifier) {
	sendChannelData(serverConnectionHandlerID, channelID, newChannelParentID);
}

void ts3plugin_onUpdateChannelEvent(uint64 serverConnectionHandlerID, uint64 channelID) {
	//sendChannelData(serverConnectionHandlerID, channelID);
}

void ts3plugin_onUpdateChannelEditedEvent(uint64 serverConnectionHandlerID, uint64 channelID, anyID invokerID, const char* invokerName, const char* invokerUniqueIdentifier) {
	sendChannelData(serverConnectionHandlerID, channelID, MAXUINT64);
}

void ts3plugin_onUpdateClientEvent(uint64 serverConnectionHandlerID, anyID clientID, anyID invokerID, const char* invokerName, const char* invokerUniqueIdentifier) {
	//char s[150];
	//std::sprintf(s,"UpdateClientEvent| clientID: %u, invokerID: %u, invokerName: %s, invokerUniqueIdentifier: %s", clientID, invokerID, invokerName, invokerUniqueIdentifier);
	//ts3Functions.printMessageToCurrentTab(s);
}

void ts3plugin_onClientMoveEvent(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility, const char* moveMessage) {
	sendClientData(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility);
}

void ts3plugin_onClientMoveSubscriptionEvent(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility) {
	sendClientData(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility);
}

void ts3plugin_onClientMoveTimeoutEvent(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility, const char* timeoutMessage) {
	sendClientData(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility);
}

void ts3plugin_onClientMoveMovedEvent(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility, anyID moverID, const char* moverName, const char* moverUniqueIdentifier, const char* moveMessage) {
	sendClientData(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility);
}

void ts3plugin_onClientKickFromChannelEvent(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility, anyID kickerID, const char* kickerName, const char* kickerUniqueIdentifier, const char* kickMessage) {

	sendClientData(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility);
}

void ts3plugin_onClientKickFromServerEvent(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility, anyID kickerID, const char* kickerName, const char* kickerUniqueIdentifier, const char* kickMessage) {
	sendClientData(serverConnectionHandlerID, clientID, oldChannelID, newChannelID, visibility);
}

void ts3plugin_onClientIDsEvent(uint64 serverConnectionHandlerID, const char* uniqueClientIdentifier, anyID clientID, const char* clientName) {
	/*char s[200];
	std::sprintf(s, "ClientIDsEvent| clientID: %u, clientName: %s, moverUniqueIdentifier: %s", clientID, clientName, uniqueClientIdentifier);
	ts3Functions.printMessageToCurrentTab(s);*/

}

void ts3plugin_onClientIDsFinishedEvent(uint64 serverConnectionHandlerID) {
}

void ts3plugin_onServerEditedEvent(uint64 serverConnectionHandlerID, anyID editerID, const char* editerName, const char* editerUniqueIdentifier) {
}

void ts3plugin_onServerUpdatedEvent(uint64 serverConnectionHandlerID) {
}

int ts3plugin_onServerErrorEvent(uint64 serverConnectionHandlerID, const char* errorMessage, unsigned int error, const char* returnCode, const char* extraMessage) {
	printf("PLUGIN: onServerErrorEvent %llu %s %d %s\n", (long long unsigned int)serverConnectionHandlerID, errorMessage, error, (returnCode ? returnCode : ""));
	if(returnCode) {
		/* A plugin could now check the returnCode with previously (when calling a function) remembered returnCodes and react accordingly */
		/* In case of using a a plugin return code, the plugin can return:
		 * 0: Client will continue handling this error (print to chat tab)
		 * 1: Client will ignore this error, the plugin announces it has handled it */
		return 1;
	}
	return 0;  /* If no plugin return code was used, the return value of this function is ignored */
}

void ts3plugin_onServerStopEvent(uint64 serverConnectionHandlerID, const char* shutdownMessage) {
}

int ts3plugin_onTextMessageEvent(uint64 serverConnectionHandlerID, anyID targetMode, anyID toID, anyID fromID, const char* fromName, const char* fromUniqueIdentifier, const char* message, int ffIgnored) {
    //printf("PLUGIN: onTextMessageEvent %llu %d %d %s %s %d\n", (long long unsigned int)serverConnectionHandlerID, targetMode, fromID, fromName, message, ffIgnored);

	if (targetMode == TextMessageTarget_CLIENT || targetMode == TextMessageTarget_CHANNEL && fromID != selfClientIds[serverConnectionHandlerID]) {
		PackageRevMessage * package = new PackageRevMessage();
		package->_PID = PID::REVMESSAGE;
		package->serverConnectionHandlerID = serverConnectionHandlerID;
		package->MessageType = (MessageType)targetMode;
		package->TargetID = toID;
		XMemCopyChar(package->UniqueIdentifier, fromUniqueIdentifier);
		XMemCopyChar(package->Message, message);
	
		SendPackage(-1, package, sizeof(PackageRevMessage));
		delete package;
	}


	/* Friend/Foe manager has ignored the message, so ignore here as well. */
	if(ffIgnored) {
		return 0; /* Client will ignore the message anyways, so return value here doesn't matter */
	}
    return 1;  /* 0 = handle normally, 1 = client will ignore the text message */
}

void ts3plugin_onTalkStatusChangeEvent(uint64 serverConnectionHandlerID, int status, int isReceivedWhisper, anyID clientID) {
	/* Demonstrate usage of getClientDisplayName */
	/*char name[512];
	if(ts3Functions.getClientDisplayName(serverConnectionHandlerID, clientID, name, 512) == ERROR_ok) {
		if(status == STATUS_TALKING) {
			printf("--> %s starts talking\n", name);
		} else {
			printf("--> %s stops talking\n", name);
		}
	}*/
}

void ts3plugin_onConnectionInfoEvent(uint64 serverConnectionHandlerID, anyID clientID) {
}

void ts3plugin_onServerConnectionInfoEvent(uint64 serverConnectionHandlerID) {
}

void ts3plugin_onChannelSubscribeEvent(uint64 serverConnectionHandlerID, uint64 channelID) {
	sendChannelData(serverConnectionHandlerID, channelID, UINT64_MAX);
}

void ts3plugin_onChannelSubscribeFinishedEvent(uint64 serverConnectionHandlerID) {
}

void ts3plugin_onChannelUnsubscribeEvent(uint64 serverConnectionHandlerID, uint64 channelID) {
	sendChannelData(serverConnectionHandlerID, channelID, UINT64_MAX);
}

void ts3plugin_onChannelUnsubscribeFinishedEvent(uint64 serverConnectionHandlerID) {
}

void ts3plugin_onChannelDescriptionUpdateEvent(uint64 serverConnectionHandlerID, uint64 channelID) {
	sendChannelData(serverConnectionHandlerID, channelID, MAXUINT64, true);
}

void ts3plugin_onChannelPasswordChangedEvent(uint64 serverConnectionHandlerID, uint64 channelID) {
}

void ts3plugin_onPlaybackShutdownCompleteEvent(uint64 serverConnectionHandlerID) {
}

void ts3plugin_onSoundDeviceListChangedEvent(const char* modeID, int playOrCap) {
}

void ts3plugin_onEditPlaybackVoiceDataEvent(uint64 serverConnectionHandlerID, anyID clientID, short* samples, int sampleCount, int channels) {
}

void ts3plugin_onEditPostProcessVoiceDataEvent(uint64 serverConnectionHandlerID, anyID clientID, short* samples, int sampleCount, int channels, const unsigned int* channelSpeakerArray, unsigned int* channelFillMask) {
}

void ts3plugin_onEditMixedPlaybackVoiceDataEvent(uint64 serverConnectionHandlerID, short* samples, int sampleCount, int channels, const unsigned int* channelSpeakerArray, unsigned int* channelFillMask) {
}

void ts3plugin_onEditCapturedVoiceDataEvent(uint64 serverConnectionHandlerID, short* samples, int sampleCount, int channels, int* edited) {
}

void ts3plugin_onCustom3dRolloffCalculationClientEvent(uint64 serverConnectionHandlerID, anyID clientID, float distance, float* volume) {
}

void ts3plugin_onCustom3dRolloffCalculationWaveEvent(uint64 serverConnectionHandlerID, uint64 waveHandle, float distance, float* volume) {
}

void ts3plugin_onUserLoggingMessageEvent(const char* logMessage, int logLevel, const char* logChannel, uint64 logID, const char* logTime, const char* completeLogString) {
}

/* Clientlib rare */

void ts3plugin_onClientBanFromServerEvent(uint64 serverConnectionHandlerID, anyID clientID, uint64 oldChannelID, uint64 newChannelID, int visibility, anyID kickerID, const char* kickerName, const char* kickerUniqueIdentifier, uint64 time, const char* kickMessage) {
}

int ts3plugin_onClientPokeEvent(uint64 serverConnectionHandlerID, anyID fromClientID, const char* pokerName, const char* pokerUniqueIdentity, const char* message, int ffIgnored) {
    return 1;  /* 0 = handle normally, 1 = client will ignore the poke */
}

void ts3plugin_onClientSelfVariableUpdateEvent(uint64 serverConnectionHandlerID, int flag, const char* oldValue, const char* newValue) {
}

void ts3plugin_onFileListEvent(uint64 serverConnectionHandlerID, uint64 channelID, const char* path, const char* name, uint64 size, uint64 datetime, int type, uint64 incompletesize, const char* returnCode) {
}

void ts3plugin_onFileListFinishedEvent(uint64 serverConnectionHandlerID, uint64 channelID, const char* path) {
}

void ts3plugin_onFileInfoEvent(uint64 serverConnectionHandlerID, uint64 channelID, const char* name, uint64 size, uint64 datetime) {
}

void ts3plugin_onServerGroupListEvent(uint64 serverConnectionHandlerID, uint64 serverGroupID, const char* name, int type, int iconID, int saveDB) {
}

void ts3plugin_onServerGroupListFinishedEvent(uint64 serverConnectionHandlerID) {
}

void ts3plugin_onServerGroupByClientIDEvent(uint64 serverConnectionHandlerID, const char* name, uint64 serverGroupList, uint64 clientDatabaseID) {
}

void ts3plugin_onServerGroupPermListEvent(uint64 serverConnectionHandlerID, uint64 serverGroupID, unsigned int permissionID, int permissionValue, int permissionNegated, int permissionSkip) {
}

void ts3plugin_onServerGroupPermListFinishedEvent(uint64 serverConnectionHandlerID, uint64 serverGroupID) {
}

void ts3plugin_onServerGroupClientListEvent(uint64 serverConnectionHandlerID, uint64 serverGroupID, uint64 clientDatabaseID, const char* clientNameIdentifier, const char* clientUniqueID) {
}

void ts3plugin_onChannelGroupListEvent(uint64 serverConnectionHandlerID, uint64 channelGroupID, const char* name, int type, int iconID, int saveDB) {
}

void ts3plugin_onChannelGroupListFinishedEvent(uint64 serverConnectionHandlerID) {
}

void ts3plugin_onChannelGroupPermListEvent(uint64 serverConnectionHandlerID, uint64 channelGroupID, unsigned int permissionID, int permissionValue, int permissionNegated, int permissionSkip) {
}

void ts3plugin_onChannelGroupPermListFinishedEvent(uint64 serverConnectionHandlerID, uint64 channelGroupID) {
}

void ts3plugin_onChannelPermListEvent(uint64 serverConnectionHandlerID, uint64 channelID, unsigned int permissionID, int permissionValue, int permissionNegated, int permissionSkip) {
}

void ts3plugin_onChannelPermListFinishedEvent(uint64 serverConnectionHandlerID, uint64 channelID) {
}

void ts3plugin_onClientPermListEvent(uint64 serverConnectionHandlerID, uint64 clientDatabaseID, unsigned int permissionID, int permissionValue, int permissionNegated, int permissionSkip) {
}

void ts3plugin_onClientPermListFinishedEvent(uint64 serverConnectionHandlerID, uint64 clientDatabaseID) {
}

void ts3plugin_onChannelClientPermListEvent(uint64 serverConnectionHandlerID, uint64 channelID, uint64 clientDatabaseID, unsigned int permissionID, int permissionValue, int permissionNegated, int permissionSkip) {
}

void ts3plugin_onChannelClientPermListFinishedEvent(uint64 serverConnectionHandlerID, uint64 channelID, uint64 clientDatabaseID) {
}

void ts3plugin_onClientChannelGroupChangedEvent(uint64 serverConnectionHandlerID, uint64 channelGroupID, uint64 channelID, anyID clientID, anyID invokerClientID, const char* invokerName, const char* invokerUniqueIdentity) {
}

int ts3plugin_onServerPermissionErrorEvent(uint64 serverConnectionHandlerID, const char* errorMessage, unsigned int error, const char* returnCode, unsigned int failedPermissionID) {
	return 0;  /* See onServerErrorEvent for return code description */
}

void ts3plugin_onPermissionListGroupEndIDEvent(uint64 serverConnectionHandlerID, unsigned int groupEndID) {
}

void ts3plugin_onPermissionListEvent(uint64 serverConnectionHandlerID, unsigned int permissionID, const char* permissionName, const char* permissionDescription) {
}

void ts3plugin_onPermissionListFinishedEvent(uint64 serverConnectionHandlerID) {
}

void ts3plugin_onPermissionOverviewEvent(uint64 serverConnectionHandlerID, uint64 clientDatabaseID, uint64 channelID, int overviewType, uint64 overviewID1, uint64 overviewID2, unsigned int permissionID, int permissionValue, int permissionNegated, int permissionSkip) {
}

void ts3plugin_onPermissionOverviewFinishedEvent(uint64 serverConnectionHandlerID) {
}

void ts3plugin_onServerGroupClientAddedEvent(uint64 serverConnectionHandlerID, anyID clientID, const char* clientName, const char* clientUniqueIdentity, uint64 serverGroupID, anyID invokerClientID, const char* invokerName, const char* invokerUniqueIdentity) {
}

void ts3plugin_onServerGroupClientDeletedEvent(uint64 serverConnectionHandlerID, anyID clientID, const char* clientName, const char* clientUniqueIdentity, uint64 serverGroupID, anyID invokerClientID, const char* invokerName, const char* invokerUniqueIdentity) {
}

void ts3plugin_onClientNeededPermissionsEvent(uint64 serverConnectionHandlerID, unsigned int permissionID, int permissionValue) {
}

void ts3plugin_onClientNeededPermissionsFinishedEvent(uint64 serverConnectionHandlerID) {
}

void ts3plugin_onFileTransferStatusEvent(anyID transferID, unsigned int status, const char* statusMessage, uint64 remotefileSize, uint64 serverConnectionHandlerID) {
}

void ts3plugin_onClientChatClosedEvent(uint64 serverConnectionHandlerID, anyID clientID, const char* clientUniqueIdentity) {
}

void ts3plugin_onClientChatComposingEvent(uint64 serverConnectionHandlerID, anyID clientID, const char* clientUniqueIdentity) {
}

void ts3plugin_onServerLogEvent(uint64 serverConnectionHandlerID, const char* logMsg) {
}

void ts3plugin_onServerLogFinishedEvent(uint64 serverConnectionHandlerID, uint64 lastPos, uint64 fileSize) {
}

void ts3plugin_onMessageListEvent(uint64 serverConnectionHandlerID, uint64 messageID, const char* fromClientUniqueIdentity, const char* subject, uint64 timestamp, int flagRead) {
}

void ts3plugin_onMessageGetEvent(uint64 serverConnectionHandlerID, uint64 messageID, const char* fromClientUniqueIdentity, const char* subject, const char* message, uint64 timestamp) {
}

void ts3plugin_onClientDBIDfromUIDEvent(uint64 serverConnectionHandlerID, const char* uniqueClientIdentifier, uint64 clientDatabaseID) {
}

void ts3plugin_onClientNamefromUIDEvent(uint64 serverConnectionHandlerID, const char* uniqueClientIdentifier, uint64 clientDatabaseID, const char* clientNickName) {
}

void ts3plugin_onClientNamefromDBIDEvent(uint64 serverConnectionHandlerID, const char* uniqueClientIdentifier, uint64 clientDatabaseID, const char* clientNickName) {
}

void ts3plugin_onComplainListEvent(uint64 serverConnectionHandlerID, uint64 targetClientDatabaseID, const char* targetClientNickName, uint64 fromClientDatabaseID, const char* fromClientNickName, const char* complainReason, uint64 timestamp) {
}

void ts3plugin_onBanListEvent(uint64 serverConnectionHandlerID, uint64 banid, const char* ip, const char* name, const char* uid, uint64 creationTime, uint64 durationTime, const char* invokerName,
							  uint64 invokercldbid, const char* invokeruid, const char* reason, int numberOfEnforcements, const char* lastNickName) {
}

void ts3plugin_onClientServerQueryLoginPasswordEvent(uint64 serverConnectionHandlerID, const char* loginPassword) {
}

void ts3plugin_onPluginCommandEvent(uint64 serverConnectionHandlerID, const char* pluginName, const char* pluginCommand) {
	printf("ON PLUGIN COMMAND: %s %s\n", pluginName, pluginCommand);
}

void ts3plugin_onIncomingClientQueryEvent(uint64 serverConnectionHandlerID, const char* commandText) {
}

void ts3plugin_onServerTemporaryPasswordListEvent(uint64 serverConnectionHandlerID, const char* clientNickname, const char* uniqueClientIdentifier, const char* description, const char* password, uint64 timestampStart, uint64 timestampEnd, uint64 targetChannelID, const char* targetChannelPW) {
}

/* Client UI callbacks */

void ts3plugin_onAvatarUpdated(uint64 serverConnectionHandlerID, anyID clientID, const char* avatarPath) {

}

const char* ts3plugin_keyDeviceName(const char* keyIdentifier) {
	return NULL;
}

// This function translates the given key identifier to a friendly key name for display in the UI
const char* ts3plugin_displayKeyText(const char* keyIdentifier) {
	return NULL;
}

// This is used internally as a prefix for hotkeys so we can store them without collisions.
// Should be unique across plugins.
const char* ts3plugin_keyPrefix() {
	return NULL;
}

/* Called when client custom nickname changed */
void ts3plugin_onClientDisplayNameChanged(uint64 serverConnectionHandlerID, anyID clientID, const char* displayName, const char* uniqueClientIdentifier) {
	uint64 channel;
	if (ts3Functions.getChannelOfClient(serverConnectionHandlerID, clientID, &channel) == ERROR_ok)
		sendClientData(serverConnectionHandlerID, clientID, channel, channel, RETAIN_VISIBILITY);
}