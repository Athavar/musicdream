#pragma once

#include <stdio.h>
#include <winsock2.h>
#include <WS2tcpip.h>
#include <stdlib.h>
#include <string>

#include <sstream>
#include <fstream>
#include <iostream>
#include <thread>  
#include <mutex>
#include <queue>

#include <algorithm>
#include <cctype>
#include "ts3_socketinteraction.h"
#include "teamspeak/public_definitions.h"


#pragma comment(lib, "Ws2_32.lib")



const static char * SOCKETSETTINGFILEPATH = "D:\\ts3bot.conf";

enum SocketState {SOCKET_INIT, SOCKET_CONNECT, SOCKET_CLOSED};

class SocketWrapper
{
public:
	SocketWrapper();
	~SocketWrapper();
	bool init();
	bool connectServer();

	void requestHandler();
	void queueWorker();

	bool disconnect();
	bool sendData(PackageData_t * apackData, uint16_t packlength);

private:
	bool readConfig();

	SOCKET _Sock;
	SocketState _SocketState;
	struct sockaddr_in server;  

	
	uint8_t * _RecvBuffer[BUFFERLENGTH] = { 0 };
	uint8_t * _SendBuffer[BUFFERLENGTH] = { 0 };
	uint8_t _SendCounter = 0;
	std::mutex _sendMutex;

	std::mutex _ReciveQueueMutex;
	std::queue<unsigned char *> _ReciveQueue;
	std::condition_variable _NewReciveData;
	
};
