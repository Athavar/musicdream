#include "Serialize.h"
#include "ts3_socketinteraction.h"



Serialize::Serialize()
{}


Serialize::~Serialize()
{}

unsigned char * Serialize::serialize_uint32(unsigned char *buffer, uint32_t value)
{
	/* Write big-endian int value into buffer; assumes 32-bit int and 8-bit char. */
	buffer[0] = value >> 24;
	buffer[1] = value >> 16;
	buffer[2] = value >> 8;
	buffer[3] = value;
	return buffer + 4;
}
uint32_t Serialize::deserialize_uint32(unsigned char *buffer)
{
	return (((uint32_t)buffer[0] >> 24) & 0xff) | (((uint32_t)buffer[1] >> 16) & 0xff) | (((uint32_t)buffer[2] >> 8) & 0xff) | ((uint32_t)buffer[3] & 0xff);
}

unsigned char * Serialize::serialize_uint16(unsigned char *buffer, uint16_t value)
{
	buffer[1] = (value >> 8) & 0xff;
	buffer[0] = value  & 0xff;
	return buffer + 1;
}
uint16_t Serialize::deserialize_uint16(unsigned char *buffer)
{
	return (uint16_t) buffer[0]|(buffer[1]<<8);;
}

unsigned char * Serialize::serialize_uint8(unsigned char *buffer, uint8_t value)
{
	buffer[0] = value;
	return buffer + 1;
}
uint8_t Serialize::deserialize_uint8(unsigned char *buffer)
{
	return (uint8_t)buffer[0] & 0xff;
}

#pragma endregion


struct PackageConnect Serialize::deserialize_PackageConnect(unsigned char *buffer)
{
	PackageConnect package;
	package.pID = (PID)deserialize_uint8(buffer); buffer += 1;
	memcpy(&package.IpAddress, buffer, 255); buffer += 255;
	package.Port = deserialize_uint16(buffer); buffer += 2;
	memcpy(&package.Identity, buffer, 28); buffer += 28;
	memcpy(&package.Nickname, buffer, 28); buffer += 28;
	memcpy(&package.ServerPassword, buffer, 40); buffer += 40;
	return package;
}