#pragma once
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>

class Serialize
{
public:
	Serialize();
	~Serialize();

	static unsigned char * serialize_uint32(unsigned char *buffer, uint32_t value);
	static uint32_t deserialize_uint32(unsigned char *buffer);

	static unsigned char * serialize_uint16(unsigned char *buffer, uint16_t value);
	static uint16_t deserialize_uint16(unsigned char *buffer);

	static unsigned char * serialize_uint8(unsigned char *buffer, uint8_t value);
	static uint8_t deserialize_uint8(unsigned char *buffer);


	static struct PackageConnect deserialize_PackageConnect(unsigned char *buffer);

#pragma endregion


};

