#include "SocketWrapper.h"
#include "Serialize.h"
#include "plugin.h"

#include <thread>  


SocketWrapper::SocketWrapper()
{
}


SocketWrapper::~SocketWrapper()
{}

bool SocketWrapper::init()
{
	if (!readConfig())
	{
		return false;
	}
	return true;
}

bool SocketWrapper::readConfig()
{

	printf("\nread Config for SOCKET...");
	std::string line;
	std::ifstream infile(SOCKETSETTINGFILEPATH);
	if (!infile.good())
	{
		printf("\ncan't read file \"%s\"",SOCKETSETTINGFILEPATH);
		return false;
	}

	memset(&server, '0', sizeof(server));

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	if (!std::getline(infile, line) || inet_pton(AF_INET, line.c_str(), &server.sin_addr)<=0)
	{
		printf("Invalid address/ Address not supported in the first line of config");
		return false;
	}

	if (!std::getline(infile, line) || line.empty() || line.length() > 5 || std::find_if(line.begin(), line.end(), [](char c) { return !std::isdigit(c); }) != line.end() || std::stoul (line,nullptr,10) > 65535)
	{
		printf("Invalid Port/ Port not supported in the second line of config");
		return false;
	}
	u_short val = (u_short)std::stoul(line, nullptr, 10);

	server.sin_port = htons( val );
	_SocketState = SOCKET_INIT;
	return true;
}

bool SocketWrapper::connectServer()
{
	WSADATA wsa;

	printf("\nInitialising Winsock...");
	if (WSAStartup(MAKEWORD(2,2),&wsa) != 0)
	{
		printf("Failed. Error Code : %d",WSAGetLastError());
		return false;
	}
	printf("Initialised.\n");

	_Sock = socket(AF_INET , SOCK_STREAM , 0);
	if (_Sock == SOCKET_ERROR)
	{
		printf("Could not create socket. Error Code : %d", WSAGetLastError());
		return false;
	}
	puts("Socket created");
	while (_SocketState != SOCKET_CLOSED)
	{
		while (_SocketState == SOCKET_INIT)
		{

			//Connect to remote server
			if (connect(_Sock , (struct sockaddr *)&server , sizeof(server)) == 0)
			{
				_SocketState = SOCKET_CONNECT;
				break;
			}
			Sleep(1000);
		}

		puts("Connected\n");

		requestHandler();
	}
	return true;
}

void SocketWrapper::requestHandler()
{
	int iResult;
	int usedBufferSize = 0;
	PackageHeader_t header = {EMPTY, 0, 0};
	while (_SocketState == SOCKET_CONNECT)
	{
		iResult = recv(_Sock, (char *)_RecvBuffer, BUFFERLENGTH, 0);
		if (iResult == SOCKET_ERROR)
		{
			printf("Failed. Error or recive data Error Code : %d", WSAGetLastError());
			return;
		}
		else if (iResult == 0)
		{
			printf("iResult == 0");
			continue;
		}


		
		if (header.pID == EMPTY && iResult == HEADERSIZE)
		{
				memcpy(&header, _RecvBuffer, HEADERSIZE);
		} else if (header.length == iResult)
		{
			unsigned char * data = (unsigned char *) malloc(header.length);
			memcpy(data, _RecvBuffer, header.length);

			std::unique_lock<std::mutex> lck(_ReciveQueueMutex);
			_ReciveQueue.emplace(data);
			lck.unlock();
			_NewReciveData.notify_one();
			header.pID = EMPTY;
		} else {
			header.pID = EMPTY;
		}
	}
	if(_SocketState != SOCKET_CLOSED)
		_SocketState = SOCKET_INIT;
	
}

void SocketWrapper::queueWorker()
{
	while (_SocketState != SOCKET_CLOSED)
	{
		std::unique_lock<std::mutex> lck(_ReciveQueueMutex);
		_NewReciveData.wait(lck);
		if (_ReciveQueue.empty())
		{
			break;
		}
		while (!_ReciveQueue.empty())
		{
			unsigned char * data = _ReciveQueue.front();
			_ReciveQueue.pop();

			switch ((PID)(uint8_t)data[0])
			{
			case STARTCONNECTION:
			{
				PackageConnect packageConnect = Serialize::deserialize_PackageConnect(data);
				char * ip = packageConnect.IpAddress;
				uint16_t port = packageConnect.Port;
				if (ERROR_ok == ts3Functions.startConnection(connectetServerBotHandlerID, packageConnect.Identity, packageConnect.IpAddress, packageConnect.Port, packageConnect.Nickname, NULL, "", packageConnect.ServerPassword))
				{
					PackageConnectReply * PackageConnectReply = (struct PackageConnectReply *) malloc(sizeof(PackageConnectReply));
					PackageConnectReply->pID = STARTCONNECTIONREPLY;
					PackageConnectReply->_BotConnectionState = CONNECTED;
					sendData(PackageConnectReply, sizeof(PackageConnectReply));
				}
				printf("get Package pid CONNECT %s %u", ip, port);

				break;
			}
			default:
				break;
			}

		}
	}
	return;
}


bool SocketWrapper::disconnect()
{
	if (_SocketState == SOCKET_CLOSED) return false;

	_SocketState = SOCKET_CLOSED;
	if (shutdown(_Sock, SD_RECEIVE) == SOCKET_ERROR)
	{
		printf("shutdown socket Failed. Error Code : %d",WSAGetLastError());
	}
	if(closesocket(_Sock) == SOCKET_ERROR)
	{
		printf("close socket Failed. Error Code : %d",WSAGetLastError());
	}
	if (WSACleanup() != 0)
	{
		printf("WSACleanup Failed. Error Code : %d",WSAGetLastError());
	}
	//To exit queueWorker Thread
	_NewReciveData.notify_all();
	return true;
}

bool SocketWrapper::sendData(PackageData_t * packData, uint16_t packlength)
{

	if (_SocketState != SOCKET_CONNECT) 
	{
		printf("send failed because socket isn't connected");
		return false;
	}
	if (HEADERSIZE + packlength > BUFFERLENGTH)
	{
		printf("send failed because package is too large");
		return false;
	}

	int iResult;
	_sendMutex.lock();
	*_SendBuffer[0] = packData->pID;
	*_SendBuffer[1] = _SendCounter;
	*_SendBuffer[2] = packlength >> 8;
	*_SendBuffer[3] = packlength & 0xff;
	memcpy(_SendBuffer + HEADERSIZE, packData, packlength);
	_SendCounter++;
	iResult = send(_Sock, (char *)_SendBuffer, HEADERSIZE + packlength, 0);
	_sendMutex.unlock();
	if (iResult == SOCKET_ERROR)
	{
		printf("send failed with error: %d\n", WSAGetLastError());
		return false;
	}
	return true;
}

/*
[DllImport("user32.dll")]
static HWND FindWindow(LPCTSTR lpClassName, LPCTSTR lpWindowName);
//public static extern int FindWindow(string lpClassName, string lpWindowName);
[DllImport("user32.dll")]
static HWND SendMessage(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
//public static extern int SendMessage(int hWnd, uint Msg, int wParam, int lParam);
#define WM_COMMAND = 0x0112;
#define WM_CLOSE = 0xF060;
*/